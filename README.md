Docs pages for FMM and constant pH

# For the visitors

CI deploys the page there:

https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/


# For the writers


## Style guide

- Figure in a figure environement, with the nice caption box (copy one from another page)
- Purely decorative figure (see beginning of the titration tutorial) inline, without border or caption.



## Templates

The following templates are defined to include images and iframe in way which is
independent of the base URL where the site is deployed (locally, on gitlab pages, or
elsewhere). They are defined in `/layouts/shortcodes/`. See Hugo documentation on
short codes to define your own.



## Template: baseimg1

To insert an image. 

```
{{< baseimg1 "/images/prot_frag_his_deprot.png" "general_figure_image" "Deprotonated histidine sidechain" "45%" "inline" >}}
```

First argument `/images/prot_frag_his_deprot.png` is absolute path (except for the base URL part) of the image. Second argument `general_figure_image` is the CSS style class (this class is good for most figures). Third argument is the HTML alt-text, which should be filled with a short description of the image for accessibility purpose (screen reader). The fourth argument, which can be left blank (`""`) is the CSS width, directly passed to `width: ARG`. Therefore, various CSS length specifier, like `45%` or `100px` or `10vw` can be used as needed. The final argument is passed to CSS `display: ARG`. Leave blank for the typical block case, and use `inline` when multiple image on the same line are placed, or images are placed within the flow of the text.


## Template: iframeviz1

To insert interactive visualization as an iframe (the visualization are full HTML page in
their own right).

```
{{< iframeviz1 "viz/cph_explain_env.html" 430 >}}
```

First argument is the path to the HTML file, second is the width in pixel of the iframe.


