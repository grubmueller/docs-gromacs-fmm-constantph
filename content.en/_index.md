---
title: "GROMACS FMM/CpH"
description: "Documentation and tutorials for the GROMACS FMM & Constant pH Codes"
---

![Logo of FMM Constant pH implementation](./images/FMM_ConstantPH_t.png)

# GROMACS FMM & Constant pH

This website contains documentation and tutorials for the GROMACS Fast Multipole Method (FMM) and Constant pH MD code. Our software is a fork of the [GROMACS](https://www.gromacs.org/) Molecular Dynamics package, enabling FMM electrostatics and dynamic protonation (constant pH) MD simulations for biomolecular system simulations.

<div class="box_note">
<div class="box_note_heading">Website under construction</div>
<div class="box_note_text">
Please note that some information may be missing, outdated, or inaccurate for the current version of the code.
</div>
</div>



## Getting Started

We recommend the following resources:

- Install our FMM & Constant pH GROMACS implementation using our [installation guide]({{< relref "/docs/install_guide" >}})
- Download [a constant pH-enabled force field]({{< relref "/docs/cph/forcefields" >}})
- Explore Constant pH simulations with our [GROMACS Lysozyme tutorial]({{< relref "/docs/tutorials/cph-lysozyme-tuto" >}})
- Additional tutorials and examples coming soon

## Credits


This software package was made possible by work by members of the [Theoretical and Computational Biophysics group](https://www.mpinat.mpg.de/grubmueller) led by Prof. Dr. Helmut Grubmüller at the [Max Planck Institute for Multidisciplinary Science (MPI-NAT)](https://www.mpinat.mpg.de), part of the network of institutes of the [Max Planck Society](https://www.mpg.de/en).


If you find this software useful, please cite the following article:


- [Constant pH Simulation with FMM Electrostatics in GROMACS. (A) Design and Applications](https://pubs.acs.org/doi/10.1021/acs.jctc.4c01318)
- [Constant pH Simulation with FMM Electrostatics in GROMACS. (B) GPU Accelerated Hamiltonian Interpolation](https://pubs.acs.org/doi/10.1021/acs.jctc.4c01319)


```bibtex
@article{briand2024constant,
  title={Constant pH Simulation with FMM Electrostatics in GROMACS.(A) Design and Applications},
  author={Briand, Eliane and Kohnke, Bartosz and Kutzner, Carsten and Grubmüller, Helmut},
  journal={Journal of Chemical Theory and Computation},
  year={2024},
  publisher={ACS Publications}
}

@article{kohnke2024constant,
  title={Constant pH Simulation with FMM Electrostatics in GROMACS.(B) GPU Accelerated Hamiltonian Interpolation},
  author={Kohnke, Bartosz and Briand, Eliane and Kutzner, Carsten and Grubmüller, Helmut},
  journal={Journal of Chemical Theory and Computation},
  year={2024},
  publisher={ACS Publications}
}


```


### Supplementary informations

Non-pdf files accompagnying the preprints

- Force fields distribution: see our [a constant pH-enabled force field]({{< relref "/docs/cph/forcefields" >}})
- [PDB files and MDP templates used in these manuscripts](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/mdp_template_titr.tar.gz) (Search and replace `$$$$` with the relevant parameters)

