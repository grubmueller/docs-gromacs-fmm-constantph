---
title: Contact
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main
---

<div class="maintitle">Contact / Feedback</div>

Thanks for your interest in our software! You can contact us in the following ways:

# Email the authors

To avoid spam, our email address are not present on this website, but you can find them on [our department webpage](https://www.mpinat.mpg.de/grubmueller/gromacs-fmm-constantph). Don't hesitate to contact us, we welcome any feedback on our software or this website!


# Forum

Since this constant pH implementation is not affiliated with the official GROMACS codebase, we ask that you **refrain from posting about problem you might encounter with this code on the GROMACS forum**. (However, problems that are generic to GROMACS, and not specific to constant pH features, are of course fair game).

# Logo usage

Feel free to use the project logos for your talks or posters (right click to save).

{{< baseimg1 "/images/FMM_ConstantPH_t.png" "general_figure_image" "Both logos of the FMM and constant pH project" "50%" "block" >}}

{{< baseimg1 "/images/FMM_ConstantPH_t_text.png" "general_figure_image" "Both logos of the FMM and constant pH project, with project title below" "50%" "block" >}}


{{< baseimg1 "/images/ConstantPH_t.png" "general_figure_image" "logo of the constant pH project" "40%" "block" >}}


{{< baseimg1 "/images/FMM_t.png" "general_figure_image" "logo of the constant pH project" "40%" "block" >}}
