---
date: 2024-01-12T00:00:01+01:00
description: "FMM MDP parameters"
tags: ["fmm"]
title: "FMM MDP parameter"
weight: 100
---


<div class="maintitle">FMM MDP Parameters</div>

This page describes the parameters in GROMACS MDP files for configuring the Fast Multipole Method (FMM) electrostatic solver.

Briefly:

```
coulombtype                  = FMM      ; Replace "PME" with FMM for coulombtype
fmm-override-multipole-order = 8        ; For most cases: PME-equivalent accuracy
fmm-override-tree-depth      =          ; Depends on the system size, see below
```


# Replacing PME: `coulombtype`

To enable FMM electrostatics, set the `coulombtype` MDP parameter to `FMM` instead of `PME`.

The following parameters (non-exhaustive list) are specific to PME, and will do nothing when FMM electrostatics is used. They can nonetheless be kept in the MDP file without causing errors.

```
rcoulomb                = 1.0 
pme_order               = 4         
fourierspacing          = 0.16  
coulomb-modifier        = None
epsilon-r               = 1
ewald-rtol              = 1e-5
```

# Hierarchical decomposition depth `d`

<!-- As explained in [Intro to FMM]({{< relref "/docs/fmm/intro-fmm" >}}), --> The FMM uses hierarchical decomposition to separate particles that are close (near field), or far (far field) from each others. The number of degrees in this subdivision, i.e., the depth `d`, affects how this decomposition is done.


<figure class="general_figure">
    {{< baseimg1 "/images/CUDAFMM_publifigure_tree_depth_t.png" "general_figure_image" "Octree and depth of decomposition in FMM" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Octree decomposition in FMM</span>
    from: Kohnke B, Kutzner C, Beckmann A, et al. <a href="https://journals.sagepub.com/doi/full/10.1177/1094342020964857">A CUDA fast multipole method with highly efficient M2L far field evaluation.</a> The International Journal of High Performance Computing Applications. 2021. <a href="https://creativecommons.org/licenses/by/4.0/">CC-BY 4.0</a>
    </figcaption>
</figure>


This setting **affects performance**, but not accuracy, for a given multipole expansion order `p` (details below). Larger systems with more particles require higher depth values for efficient execution, and the optimal value may depend on your GPU hardware. Set the depth, a positive integer, using:

```scheme {linenos=false,style="monokailight"}
fmm-override-tree-depth =  2
```

To optimize this parameter, run short benchmarks over a range of values of `d`. 
Use the table below as a starting point for optimal `d`. Test the simulation speed (ns/day) with `d`, `d-1`, and `d+1`, choosing the value corresponding to the fastest simulation, or extending the benchmark if the speed trend suggests so.


| Particle count  | Typical protein box size | Typical d   |
|-----------------|--------------------------|-------------|
| < 10000 (<10k)  | Vacuum                   | **0** or 1  |
| 10k             | 5 x 5 x 5 nm             | **1**       |
| 20k             | 6 x 6 x 6 nm             | **1** or 2  |
| 30k - Lysozyme  | 7 x 7 x 7 nm             | **2**       |
| 50k - SNase     | 8 x 8 x 8 nm             | 2 or **3**  |
| 100k            |                          | 2 or **3**  |
| 500k            |                          | **3**       |
| > 1M - Ribosome |                          | 3 or **4**  |
| > 10M           |                          | >= **5**    |
| > 100M          |                          | >= **6**    |

A depth of 0 indicates all interactions occur in the near field (direct Coulomb calculation), which is very slow. This is used only for code verification or toy systems in vacuum.



# Mutipole expansion order `p`

Electrostatic interactions in FMM are computed directly for nearby particles or through multipole expansion for those in the far field.

The expansion order `p` directly **affects computational accuracy**, with higher orders yielding higher accuracy.
However, increasing the multipole order also increases computational requirements. 
MD simulations can be run in single or double precision, with a precision-dependent threshold beyond which increasing multipole expansion order becomes  futile. 


The multipole expansion order is a positive integer, set with this parameter:

```scheme {linenos=false,style="monokailight"}
fmm-override-multipole-order = 8
```

We have explored the accuracy/computation trade-off in our [GPU FMM publications](https://pubs.acs.org/doi/10.1021/acs.jctc.0c00744), including what settings will produce equivalent accuracy to the usual PME settings.


<figure class="general_figure">
    {{< baseimg1 "/images/GMXFMM_publifigure_accuracy_pme_fmm.jpeg" "general_figure_image" "Multipole order and absolute error on forces in PME and FMM, single and double precision" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Absolute error of force (x,y,z components)</span> from: Kohnke, B., Kutzner, C., & Grubmüller, H. <a href="https://pubs.acs.org/doi/10.1021/acs.jctc.0c00744">A GPU-Accelerated Fast Multipole Method for GROMACS: Performance and Accuracy.</a> Journal of Chemical Theory and Computation. 2020. <a href="https://pubs.acs.org/page/policy/authorchoice_ccby_termsofuse.html">CC-BY (ACS)</a>
    </figcaption>
</figure>



Many users aim to match the accuracy of GROMACS default PME electrostatics solver settings when using FMM.
**We recomment the `p=8` setting in both single and double precision MD** to achieve equivalency with the default PME  setting:

```scheme {linenos=false,style="monokailight"}
fmm-override-multipole-order = 8
```

If you are using non-standard PME/Ewald settings, because you have specific electrostatic accuracy needs, this setting might not be sufficient. We refer you to the aforementioned plot and article to make an informed choice.



# Special usecase settings

The previous settings apply to any simulation using FMM. There are additional usecases enabled that FMM that might also be of interest.


## Sparse FMM

For sparse systems, i.e., systems that are heterogenous with dense regions and vacuum regions, like droplets or jet, sparse FMM might yield better performance than non-sparse FMM, and also PME. The sparse mode skips over empty space efficiently, whereas dense FMM, or PME has approximately the same computational load independently of particle density, at least for the far field.


<figure class="general_figure">
    {{< baseimg1 "/images/GMXFMM_publifigure_example_sparse.jpeg" "general_figure_image" "Droplet in vacuum system, a paradigmatic sparse system" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Example of sparse system: the droplets-in-vacuum system</span> from: Kohnke, B., Kutzner, C., & Grubmüller, H. <a href="https://pubs.acs.org/doi/10.1021/acs.jctc.0c00744">A GPU-Accelerated Fast Multipole Method for GROMACS: Performance and Accuracy.</a> Journal of Chemical Theory and Computation. 2020. <a href="https://pubs.acs.org/page/policy/authorchoice_ccby_termsofuse.html">CC-BY (ACS)</a>
    </figcaption>
</figure>





Enable sparse FMM with the following setting:

```scheme {linenos=false,style="monokailight"}
fmm-sparse = yes
```

How much of a performance gain one can get is dependent on exactly how sparse the system is, therefore we recommend benchmarking with this settings enabled and disabled.

See also: [Sparse FMM Benchmarks]({{< relref "/docs/fmm/benchmarks#Sparse" >}})

## Open boundary simulations

The PME electrostatics method works only for systems with Periodic Boundary Conditions (PBC), due to its reliance on the Fourier Transform. On the other hand, FMM has no dependency on intrisically periodic primitives, and so can treat the open boundary case easily.

To enable open boundary mode, switch from:
```scheme {linenos=false,style="monokailight"}
pbc                      = xyz
```

to 

```scheme {linenos=false,style="monokailight"}
pbc                      = no
```

When using open boundary mode, it is generally necessary to disable dipole compensation with the following setting:

```scheme {linenos=false,style="monokailight"}
fmm-dipole-compensation = no
```

Note that this settings should never be set to `no` apart from this case, as it breaks the equivalence with PME tinfoild infinity boundary condition.


Additionally, the environement variable `OPENBOUNDARY` can be used for a peculiar type of simulation, where the system is formally periodic (`pbc = xyz`), but the electrostatics are treated with open boundary. This introduces less-than-physical behavior at the edge of the system, but allows correct treatment in the center of the box. This is useful when simulating a single droplet in vacuum, centered in the box, where water might evaporate and reach the edge of the box; and pretty much unused apart from this specific case.

To use this mode, set `pbc = xyz` and run `gmx mdrun` with the environement variable:

```bash
OPENBOUNDARY=1 gmx mdrun ...
```

or as a permanent setting until the shell is restarted:
```bash
export OPENBOUNDARY=1
```

# Comparing PME and FMM

When comparing PME and FMM energies, set the following parameter:

```
coulomb-modifier = None
```

This settings concerns the absolute value offset of the electrostatic energy - to get the same values for the energy with FMM and PME, PME need to run with this setting, and not with a potential switch or shift.
Because this settings does not modify forces, it does not affect the dynamics, but only energy values.


