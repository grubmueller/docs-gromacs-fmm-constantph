---
date: 2024-01-12T00:00:01+01:00
description: "FMM Benchmarks"
tags: ["fmm"]
title: "FMM Benchmarks"
weight: 100
---


<div class="maintitle">FMM Benchmarks</div>



# On GTX 1080


# On RTX 3090

# Double precision

As GROMACS double precision does not support GPU-acceleration yet, FMM will generally be significantly faster than PME in double precision, for single node performance.

# Sparse

Sparse FMM benchmarks here


# For constant pH-enabled FMM
