---
title: "Fast Multipole Method"
date: 2017-03-02T12:00:00-05:00
---

<div class="maintitle">GPU Fast Multipole Method (FMM) in GROMACS</div>



 <!--- [Intro to FMM]({{< relref "/docs/fmm/intro-fmm" >}}) for an overview of the FMM  -->
- [MDP parameters]({{< relref "/docs/fmm/fmm_parameters" >}}) documents the parameters of the GPU FMM in GROMACS
 <!--- [Benchmarks]({{< relref "/docs/fmm/benchmarks" >}}) showcases the performance of the GPU FMM in GROMACS -->



