---
date: 2017-04-09T01:00:00-00:00
description: "Introduction to FMM"
tags: ["fmm"]
title: "Intro to FMM"
weight: 100
---


<div class="maintitle">Introduction to FMM</div>


We recommend the following [short introductory material](https://math.nyu.edu/~greengar/shortcourse_fmm.pdf) to get familiar with the mathematical formulation of the Fast Multipole Method.


# Background 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc faucibus vitae risus ac scelerisque. Etiam rutrum, diam vel imperdiet bibendum, erat nisi gravida mi, vel venenatis dolor libero sed est. Aenean sapien felis, commodo vitae ipsum ut, feugiat aliquam augue. In hac habitasse platea dictumst. Proin lacus risus, tempor a magna id, laoreet efficitur lacus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam blandit fermentum elit, non ultricies justo tincidunt vitae. Donec vestibulum lorem erat, eget volutpat nunc efficitur sed. Cras laoreet pretium erat et luctus. In dignissim, diam vel molestie luctus, sapien quam maximus risus, vel varius eros tortor nec magna. Proin porta massa ut mi blandit, in congue libero tincidunt. Sed dapibus dolor sagittis elit iaculis tincidunt. Duis sagittis purus ac mauris malesuada congue. Sed quis libero fermentum, eleifend leo sed, feugiat enim. Etiam tempus convallis felis eget convallis. Maecenas urna arcu, ornare auctor molestie non, sodales id nunc.



<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/CUDAFMM_publifigure_FMM_overview_t.png" "general_figure_image" "The different computation phase in FMM represented as a circular graph, with illustration for each phase" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >The different phases of the FMM</span>
    from: Kohnke B, Kutzner C, Beckmann A, et al. <a href="https://journals.sagepub.com/doi/full/10.1177/1094342020964857">A CUDA fast multipole method with highly efficient M2L far field evaluation.</a> The International Journal of High Performance Computing Applications. 2021. <a href="https://creativecommons.org/licenses/by/4.0/">CC-BY 4.0</a>
    </figcaption>
</figure>


# Hierarchical decomposition


<figure class="general_figure">
    {{< baseimg1 "/images/CUDAFMM_publifigure_tree_depth_t.png" "general_figure_image" "Octree and depth of decomposition in FMM" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Octree decomposition in FMM</span>
    from: Kohnke B, Kutzner C, Beckmann A, et al. <a href="https://journals.sagepub.com/doi/full/10.1177/1094342020964857">A CUDA fast multipole method with highly efficient M2L far field evaluation.</a> The International Journal of High Performance Computing Applications. 2021. <a href="https://creativecommons.org/licenses/by/4.0/">CC-BY 4.0</a>
    </figcaption>
</figure>

Nullam sed neque a erat lobortis dignissim dictum vitae lacus. Integer vehicula quam eu sapien venenatis, blandit sagittis risus feugiat. Donec semper est at turpis ornare, id vehicula libero tincidunt. Nulla posuere nisi quis scelerisque consequat. Fusce tempor varius lobortis. Quisque libero eros, sodales in quam id, gravida pretium magna. Vestibulum vitae commodo est, a bibendum massa. Vestibulum dolor risus, rutrum ut lacus vitae, vehicula tristique ligula. Vivamus egestas ante ut ipsum tristique hendrerit. Vivamus lobortis vitae dui vitae finibus. Mauris auctor vitae eros et tristique. Sed scelerisque tristique quam, id tincidunt tellus congue non. Fusce iaculis tempus accumsan. Vestibulum consequat tellus sit amet rutrum condimentum.

Nunc vel ipsum est. Nunc a suscipit ante. Curabitur diam tortor, ullamcorper eu scelerisque non, suscipit non enim. Vestibulum nisl libero, finibus mollis mattis sed, rutrum at tellus. Nam dignissim erat ac lectus euismod porttitor. Donec semper orci elit, quis semper tellus cursus nec. Sed porta dapibus justo, a elementum leo dictum a. Cras mattis posuere est, in pulvinar ex congue eu. Suspendisse lacinia posuere imperdiet. Aliquam at lectus pharetra, vestibulum libero eget, facilisis nisl. Donec sed porttitor dui, vitae aliquam orci. Nulla a tincidunt purus. Pellentesque lectus mauris, facilisis ac velit et, lobortis pellentesque ipsum. Ut pharetra purus sed nulla tristique aliquet.

# Multipole expansion

Nulla venenatis, orci sit amet commodo gravida, risus purus tempus neque, et ornare libero ante non sem. Duis congue erat diam, non pulvinar urna scelerisque finibus. Sed quam leo, imperdiet vel porttitor pretium, auctor quis sem. Duis eget laoreet quam. Ut sed iaculis justo, in auctor mauris. Quisque posuere convallis nisl, eget aliquet elit porta eget. Ut tincidunt massa sed tristique eleifend. Nulla finibus lorem a magna fringilla feugiat. Mauris sapien augue, tincidunt interdum imperdiet vitae, lobortis et quam. Etiam sagittis viverra elit, id sagittis nibh semper sed. Mauris libero neque, faucibus ac pretium quis, finibus quis arcu. Etiam volutpat neque ac libero tempor, a tempus ipsum sagittis. Quisque vitae enim id sem finibus ultrices. Curabitur aliquet consequat quam, vitae laoreet risus porta ac. Duis volutpat ante et sem fermentum, sed cursus arcu euismod.

# Hierarchical decomposition

Aenean nec ligula eleifend, cursus libero et, elementum nisl. Vivamus semper nec neque eget euismod. Integer ornare imperdiet risus ac rhoncus. Fusce vel odio non mauris placerat luctus eget quis tortor. Suspendisse nec tortor a ante laoreet feugiat eleifend at dolor. Integer at sapien nisl. Curabitur scelerisque et lorem et rhoncus. Maecenas tortor ipsum, tempor ac pharetra sed, tristique id diam. Praesent sit amet sodales sem, et tincidunt metus. In blandit dapibus consectetur. Quisque quam ex, mollis vel ultrices id, finibus eu justo. 
