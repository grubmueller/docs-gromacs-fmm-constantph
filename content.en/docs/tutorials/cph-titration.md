---
date: 2024-01-15T00:00:01-00:00
description: "Computational determination of pKa values for titratable residues in hen egg white lysozyme using constant pH molecular dynamics"
featured_image: ""
tags: ["proteins", "constant-pH", "tutorial", "lysozyme"]
title: "Titration of proteins: Lysozyme"
weight: 100
---

Constant pH simulations allow computational prediction of \\( \mathrm{p}K_\mathrm{a} \\) values for titratable residues in proteins. 
In this tutorial, we demonstrate this capability by analyzing the protonation states of ten ionizable residues (glutamate, aspartate, and histidine) in hen egg white lysozyme.


{{< baseimg1 "/images/lyzozyme.png" "general_figure_image" "Cartoon representation of lysozyme protein structure with protonatable residues shown in atomic detail" "40%" "block" >}}


The prerequisite for this tutorial is basic familiarity with our constant pH implementation. For an introduction to the method, please refer to our [introductory constant pH tutorial]({{< relref "/docs/tutorials/cph-lysozyme-tuto" >}}).

# Background

A laboratory titration involves gradually adding a strong acid or base while monitoring solution pH. The \\( \mathrm{p}K_\mathrm{a} \\) value is determined from the inflection point of the pH curve plotted against added acid/base equivalents. Throughout this process, the population of titrated molecules remains constant while pH varies.

In contrast, computational approaches use independent simulations at fixed pH values spanning a broad range (typically pH 1.0 to 13.0). Each simulation tracks the protonation state of individual residues, with multiple replicas often performed to improve statistical sampling. Unlike laboratory experiments, these simulations maintain constant pH conditions without titrant addition.

From these simulations, we plot the protonation state occupancy of each residue against pH to generate titration curves:


{{< baseimg1 "/images/cardiotoxin_glu17.png" "general_figure_image" "Example of a titration curve obtained by constant pH titration" "700%;margin: auto" "block" >}}


These titration curves enable determination of individual residue \\( \mathrm{p}K_\mathrm{a} \\) values. While standard laboratory titrations yield only *macroscopic* data reflecting total proton binding across all sites, NMR (Nuclear Magnetic Resonance) spectroscopy provides *microscopic* measurements of individual residue protonation states. This makes NMR data ideal for validating our constant pH simulation results.

Full titration simulations require significant computational resources. The total simulation time scales with the product of individual simulation length, number of pH points sampled, and number of replicas per pH point. Each simulation must run long enough to achieve reasonable convergence of protein dynamics, which can require hundreds to thousands of nanoseconds for complex systems.

For this tutorial, we recommend running 5 brief replicas of 2-5 ns. While these short simulations will not produce converged titration curves, they effectively demonstrate the workflow and complete rapidly. When studying your own protein systems, adjust simulation lengths based on system-specific convergence requirements. For reference, lysozyme titration typically requires 75-200 ns per replica for adequate convergence. Often, a large number of replicas (e.g., 40) can be used instead of long individual replicas, with the caveat that quantities of interest, such as \\( \mathrm{p}K_\mathrm{a} \\), are likely to be accurate only when averaged over all replicas, not for any single short replica.
 

# Automation and starting files

A titration simulation requires an input structure (PDB file) and molecular dynamics parameter (MDP) files for each simulation phase (equilibration, production) across all pH points. Setting up these simulations involves numerous `gmx` commands, making manual preparation both laborious and prone to errors.

To automate this process, we therefore provide the `titration_v3.py` script, which is available on our [Scripts page]({{< relref "/docs/cph/scripts" >}}).

To run the script, you need:
- A YAML runfile defining all titration parameters
- A template directory containing the initial structure and MDP file templates
- A constant-pH enabled force field


The script package includes both the runfile and template directory. For the force field component, download our recommended modified `charmm36m` version from the [Forcefields]({{< relref "/docs/cph/forcefields" >}}) page.


# Examining the runfile

The titration parameters are specified in a [YAML](https://en.wikipedia.org/wiki/YAML) format configuration file. Below is an example runfile with section heading highlighted:


```yaml {hl_lines=[3,9,19,35,39,42],linenostart=1,style="monokailight"}
---
 runfileVersion: 3
 runcontrol:
    replicaNumber: 7
    lambdaEquilRunDuration: 0.1
    structureEquilRunDuration: 1
    productionRunDuration: 2
    runDir: "/lys2/"
 inputFiles:
    templateDirectory: "template_c36m"
    inputPdb: "lysozyme.pdb"
    customLdpFile: ""
    genionMDPTemplate: "genion.mdp.template"
    emMDPTemplate: "em.mdp.template"
    lambdaEquilMDPTemplate: "lambdaequil_coupled.mdp.template"
    structureEquilMDPTemplate: "structureequil_coupled.mdp.template"
    productionMDPTemplate: "titration_coupled.mdp.template"
    indexFile: ""
 systemParameters:
    boxVector: 8.0
    fmmDepth: 2
    velgenSeedBaseValue: 84521
    waterModel: "tip3p"
    prepfilesAdditionalCommands:
        - "ln -s /home/ebriand/Projects/__CPH_FF/charmm36m-lambdadyn.ff/"
    ffName: "charmm36m-lambdadyn"
    pdb2gmxAdditionalArguments: " -ignh -ldglu -ldhis -ldasp "
    sodiumName: "SOD"
    chlorideName: "CLA"
    keepawayGroup: "C-alpha"
    distResidueToBuffer: 3.0
    distProteinToBuffer: 2.0
    distBufferTolerance: 0.3
    ionConcentration: 0.150
 titration:
    phValues: [-1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0]
    mainRunBarrier: 6.0
    nstlambdaout: 250
 localGromacsLaunch:
    gromppThreads: 4
    localGromacsBinary: "/home/ebriand/Projects/gromacs_publication_builds/stable/stable_v1/bin/gmx"
 clusterJobLaunch:
    jobPrefix: "LYZ_2"
    clusterLoginNode: "moa1"
    clusterGromacsGMXRC: "/home/ebriand/Projects/gromacs_publication_builds/stable/cluster/stable_v1/bin/GMXRC"


```

The runfile is organized into distinct sections:
- The `runcontrol` section defines fundamental simulation parameters, including replica count and phase durations
- The `inputFiles` section specifies template files used to generate molecular dynamics parameters. These templates contain placeholders (like `$$$$FIXED_PH$$$$`) that are automatically replaced with simulation-specific values
- The `systemParameters` section configures the physical setup, including box dimensions, water model, and ion placement settings
- The `titration` section defines pH sampling points and constant-pH specific parameters
- The `localGromacsLaunch` section specifies the GROMACS executable location and local processing settings
- The `clusterJobLaunch` section contains settings for cluster job submission

For a detailed explanation of each parameter, refer to our comprehensive [parameter documentation]({{< relref "/docs/cph/titration_script" >}}). The next section will guide you through adapting this runfile for your environment.


# Adapting the runfile

Before running simulations, the runfile configuration should be modified to match your computing environment and specify the correct file locations. Here's how to set up each component:

## Titration directory

First, create the main simulation directory specified by the `runDir` parameter in the runfile's `runcontrol` section. From the working directory containing the script:


```bash
mkdir lyzozyme_1
```

## Cluster launch

While system preparation and preprocessing steps (`grompp`) run on the local machine, the actual simulations typically execute on a compute cluster. Since computing clusters vary significantly in their:

- Login node hostnames
- Connection methods
- Batch management systems (e.g., SLURM, SGE)
- Job submission protocols

We've separated the cluster submission logic into a companion script `titrate_cluster_launch.py`. This script contains a customizable `launchOnCluster` function that you **must** modify to match your specific cluster configuration. The runfile's `clusterJobLaunch` section supplies parameters to this function, enabling flexible job submission across different cluster environments.

You can find a sample SSH-based `launchOnCluster` implementation for SLURM systems on our [titration script documentation page]({{< relref "/docs/cph/titration_script" >}}). Before proceeding, modify your `titrate_cluster_launch.py` to implement a `launchOnCluster` function suitable for your cluster configuration.

## Local paths

Several paths in the runfile need to be updated to match your system:

1. In `systemParameters` >> `prepfilesAdditionalCommands`:
   - Update the symbolic link command to point to your `charmm36m-lambdadyn.ff` directory location
   - Feel free to add any other commands you might need (depending on your particular setup) - but none is needed for the present tutorial.

2. Configure `pdb2gmxAdditionalArguments`:
   - The example enables protonation for glutamate, histidine, and aspartate residues
   - Modify these options based on your system's requirements

3. Update GROMACS-related settings:
   - Set `localGromacsLaunch` >> `localGromacsBinary` to point to your constant pH-enabled `gmx` binary
   - Adjust `gromppThreads` to control parallel `gmx grompp` instances
     - A value of 1 is always safe
     - Can use up to your CPU core count for better performance

4. Configure cluster-specific parameters in `clusterJobLaunch`:
   - Update `clusterGromacsGMXRC` to your cluster's GROMACS environment file location
   - Set `clusterLoginNode` to your cluster's access node hostname
   - Modify other parameters as required by your `launchOnCluster` function implementation



# Pre-simulation steps

## Syntax verification

First, verify your runfile configuration using the titration script:

```bash
python titrate_v3.py check_runfile -r lyzozyme_1.yaml
```

which should give the following output:


```text {linenos=false,style="solarized-light"}
Mode: check_runfile
Loaded runfile
If no error before this line, file is fine.
```

## File preparation

Next, initialize the titration directory structure by copying template files to their appropriate simulation directories:

```bash
python titrate_v3.py prepfiles -r lyzozyme_1.yaml
```

which should produce a long output ending with this:

```text {linenos=false,style="solarized-light"}
Preparing files for point 20 (pH =  9.00)
    Creating /home/ebriand/Projects/__CPH_Runs/SimpleLambdadyn/port_script//lys2//point_0020/
    Copying required files...
       Copying file : lyzozyme.pdb
       Copying file : genion.mdp.template
       Copying file : em.mdp.template
       Copying file : lambdaequil_coupled_2.mdp.template
       Copying file : structureequil_coupled_2.mdp.template
       Copying file : titration_coupled_2.mdp.template
       Copying file : aminoacids_nmrpkas.ldp
    Changing dir to  /home/ebriand/Projects/__CPH_Runs/SimpleLambdadyn/port_script//lys2//point_0020/
      Executing in directory...  ln -s /home/ebriand/Projects/__CPH_FF/charmm36m-lambdadyn.ff/


Done preparing files successfully
```

The script creates a directory for each pH point and populates it with the necessary configuration files. Verify that the output ends with "Done preparing files successfully" before proceeding.

## Topology preparation

In this step, the topology files and initial protein structure will be generated using `pdb2gmx`

```bash
python titrate_v3.py prereplica -r lyzozyme_1.yaml
```

Depending on the number of pH point requested in the runfile, this might take some time. A successful completion will result in the following output:


```text {linenos=false,style="solarized-light"}
Done pre-replica work successfully
```

# Ion placement and energy minimization

This step solvates the systems, places ions and buffer sites, and performs energy minimization. As the first cluster-based operation, it serves to validate your `launchOnCluster` function configuration.


```bash
python titrate_v3.py replica_cluster -r lyzozyme_1.yaml
```

Monitor simulation progress using the appropriate `check_*` command. For this step, use `check_replica`:


```bash
python titrate_v3.py check_replica -r lyzozyme_1.yaml
```

A successful completion will have the following output

```text {linenos=false,style="solarized-light"}
The following failed the   replica EM completion check  (  0 items)

[]
```

Whereas, in case of failure or incomplete run, the integer index of the pH point and replica that failed will be listed, like such:


```text {linenos=false,style="solarized-light"}
[...]
Missing replica for 020,034
Missing replica for 020,035
Missing replica for 020,036
Missing replica for 020,037
Missing replica for 020,038
Missing replica for 020,039
The following failed the   replica EM completion check  (840 items)

[[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [0, 9], [0, 10], [0, 11],]
```

Running jobs will appear as failures in the completion check, but should resolve automatically once finished. The `check_*` commands thus function as progress monitors.

If `check_replica` reports failures after all cluster jobs have completed, restart the failed simulations using one of these commands:


```bash
# for local launch
python titrate_v3.py restart_replica -r lyzozyme_1.yaml
```
```bash
# Or alternatively, for cluster launch
python titrate_v3.py restart_replica_cluster -r lyzozyme_1.yaml
```

Do not proceed until the `replica` phase shows zero failures in the completion check.

Note that a local version of this step (which runs the energy minimization on the local machine, rather than the cluster) is available:

```bash
# Generally not used:
python titrate_v3.py replica -r lyzozyme_1.yaml
```


Note that this step might fails during `genion`, if the system box size is not large enough to accommodate the buffer particle at the required distance, or if the distance requirement cannot be fullfilled. See also [the relevant section in the lysozyme tutorial](docs-gromacs-fmm-constantph/docs/tutorials/cph-lysozyme-tuto/#ion-generation). In that case, delete the titration directory, recreate it, and enlarge the simulation box (`boxVector` in the YAML runfile, corresponding to the cubic box side in nanometers).

# Lambda equilibration

In this step, the protein structure is restrained (through the position restraints generated by `pdb2gmx`) while the $\lambda$ variables are free.
This allows the the protonation state to adapt to the initial conformation (and simulation pH), which is necessary as the starting value (uniformly $\lambda = 0$) might not be appropriate.

This step runs on the cluster, and is triggered by the following command:

```bash
python titrate_v3.py lambdaequil -r lyzozyme_1.yaml
```

Similarly to the previous step, the completion status of the replicas at the different pH values can be checked using:

```bash
python titrate_v3.py check_lambdaequil -r lyzozyme_1.yaml
```

And also similarly to energy minimization, you should proceed to structure equilibration below only once no incomplete replica remain. If necessary, jobs can be restarted using `restart_lambdaequil`.


# Structure equilibration

Once the protonation state is acceptable for the initial conformation, the usual structure equilibration (i.e. equilibration of the conformation of the protein) can begin. In this step, there are thus no restraint on the protein atoms, and the protonation states are also free to change.

The relevant commands for this step follow the same pattern as the previous steps:
```bash
python titrate_v3.py structureequil -r lyzozyme_1.yaml
python titrate_v3.py check_structureequil -r lyzozyme_1.yaml
python titrate_v3.py restart_structureequil -r lyzozyme_1.yaml
```

Depending on your settings, the duration of the structure equilibration might not be trivial. Therefore, if a simulation is incomplete, it might be beneficial to continue from the last checkpoint, rather than restart it in full. This is achieved like so

```bash
python titrate_v3.py continue_structureequil -r lyzozyme_1.yaml

```

Note that, in the case that no suitably named `.log` file is found, indicating that the simulation has not even started (and thus cannot be resumed), this command will start the simulation anew (acting like `restart_structureequil`). It is thus generally better to use `continue_structureequil` rather than `restart_structureequil`, unless restarting from the beginning is desired (e.g. [input file modification](##update-template-file), new constant pH software version).

# Production runs

The production step works similarly as the two previous ones:

```bash
python titrate_v3.py prod -r lyzozyme_1.yaml
python titrate_v3.py check_prod -r lyzozyme_1.yaml
python titrate_v3.py continue_prod -r lyzozyme_1.yaml

# Rarely needed:
python titrate_v3.py restart_prod -r lyzozyme_1.yaml
```

The completion of the production steps conclude the simulation itself. The data files produced are now to be analyzed.


For more details on the titration script commands, see [the dedicated page]({{< relref "/docs/cph/titration_script" >}}).

# Analysis

To continue with the analysis, install the [analysis tool]({{< relref "/docs/cph-postprocessing/analysis_tool" >}}). This tool performs quite simple frame counting operations, and could be replaced with a user-written script. However, it has been optimized to read and process the file much faster than an equivalent script. We thus recommend using for the first steps of your analysis.

A first post-processing step is

```bash
./LambdaAnalysis titration --runfile /path_to_/the/runfile/lyzozyme_1.yaml --do_transition_count 1 --output lyzozyme_1.csv
```

This commands reads in the files and counts the number of frame with $\lambda_p < 0.5$ and $\lambda_p \ge 0.5$, respectively, among other quantities. These are written to the CSV file, with separate record for each replica and each pH point.


A second step is computing Normalized Mutual Information to detect coupling:


```bash
./LambdaAnalysis mutual_information_titration --runfile /path_to_/the/runfile/lyzozyme_1.yaml --output lyzozyme_1_MI.csv
```

Having obtained `lyzozyme_1.csv` and `lyzozyme_1_MI.csv`, we can produce titration curve using [this Jupyter notebook]({{< relref "/docs/cph-postprocessing/analysis_tool" >}}#jupyter-notebook). This code is documented and commented, and produces results such as:

<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/lysozyme_1_titration_site_000.png" "general_figure_image" "Titration curves for the lysozyme example notebook" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Titration curve produced by the notebook</span>
    </figcaption>
</figure>
<br />
<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/lysozyme_1_CPH_vs_Ref_allres_ConfInt.png" "general_figure_image" "Titration curves for the lysozyme example notebook" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >pKa plot produced by the notebook</span>
    </figcaption>
</figure>

As discussed in the introduction, the very short replicas used in this tutorial do not lead to a converged titration, hence the high spread in the titration curve, and the low correlation between the obtained and reference results.

This notebook can be adapted to any other analysis operating at the level replicas. If the analysis to be performed requires the $\lambda$ trajectory ($\lambda$ value as a function of time), then each `.dat` file can be efficiently transformed into `.npy` NumPy array file with:

```bash
./LambdaAnalysis export_npy  --input /path/to/titration/directory/point_0000/prod_s0000.dat  --output array_0_0.npy
```

The format of the resulting file, and how to load `.npy` files, is described [there]({{< relref "/docs/cph-postprocessing/analysis_tool" >}}#numpy-array-for-a-single-file).


Finally, for more analysis methods, see our [**CPH Post processing**]({{< relref "/docs/cph-postprocessing" >}}) section.

# Conclusion

Many usage of constant pH MD will follow the general workflow of either this titration tutorial, or the simpler [lysozyme tutorial]({{< relref "/docs/tutorials/cph-lysozyme-tuto" >}}), at least for the simulation part.

Some enhanced sampling methods, such as replica exchange or expanded ensemble constant pH, could also be used to improve sampling and facilitate convergence (a tutorial for those methods is not yet available here).

In many cases, however, the value added for your project will reside in the post-processing and analysis, where the protonation state is put in relation with your process of interest --- an operation which is highly system-dependent, and therefore cannot easily be put into tutorial format.


# Addendum

## Special commands

The following are potentially useful features/commands of the titration script, that are not generally needed in the titration workflow.

### Update template file

A situation might arise where the template files, particularly MDP files, contain a mistake. For instance, a missing parameter generating errors during `grompp`. It is possible to rectify this by copying and overwriting the file in question to each `point_xxxx` directory, but this can be automatically done with:

```bash
python titrate_v3.py update_template -r lyzozyme_1.yaml
```

This copies all template files (mdp and pdb files, ldp file if specified in the run file). Note that this does not re-run any steps: running `update_template` should be followed by running, for instance, `structureequil`, if the change concerned the MDP file for this step.


## Optional runfile parameters

### Time step

The simulation \\( \delta t \\) is assumed to be 0.002 ps (2 fs). If it is not, set is using the `dt` parameter in the `runcontrol` section of the runfile (value in picosecond).

```yaml {hl_lines=[2],linenostart=1,style="monokailight"}
 runcontrol:
    dt: 0.002 
```

