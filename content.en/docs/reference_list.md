---
date: 2024-01-12T00:00:01+01:00
description: "Reference List"
tags: ["fmm", "cph", "publication" ]
title: "Reference list"
weight: 100
---

<div class="maintitle">Reference list</div>

<div class="box_note">
<div class="box_note_heading">Website under construction</div>
<div class="box_note_text">
Please note that this page is currently a work in progress.
</div>
</div>


Here are the details of the publications cited in this site


# Our most recent publications on FMM / constant pH


## Constant pH Simulation with FMM Electrostatics in GROMACS. (A) Design and Applications {#fmmcphA}

Authors: Eliane Briand, Bartosz Kohnke, Carsten Kutzner, and Helmut Grubmüller

[https://pubs.acs.org/doi/10.1021/acs.jctc.4c01318](https://pubs.acs.org/doi/10.1021/acs.jctc.4c01318)

## Constant pH Simulation with FMM Electrostatics in GROMACS. (B) Design and Applications {#fmmcphA}

Authors: Bartosz Kohnke, Eliane Briand, Carsten Kutzner, and Helmut Grubmüller


[https://pubs.acs.org/doi/10.1021/acs.jctc.4c01319](https://pubs.acs.org/doi/10.1021/acs.jctc.4c01319)


## A GPU-Accelerated Fast Multipole Method for GROMACS: Performance and Accuracy {#gpuFMM}

Authors: Bartosz Kohnke, Carsten Kutzner, and Helmut Grubmüller*

[https://pubs.acs.org/doi/10.1021/acs.jctc.0c00744](https://pubs.acs.org/doi/10.1021/acs.jctc.0c00744)


# Our older publications on constant pH

## Accurate Three States Model for Amino Acids with Two Chemically Coupled Titrating Sites in Explicit Solvent Atomistic Constant pH Simulations and pKa Calculations

Authors:  Plamen Dobrev, Serena Donnini, Gerrit Groenhof, and Helmut Grubmüller*

[https://pubs.acs.org/doi/10.1021/acs.jctc.6b00807](https://pubs.acs.org/doi/10.1021/acs.jctc.6b00807)

## Probing the Accuracy of Explicit Solvent Constant pH Molecular Dynamics Simulations for Peptides

Authors: Plamen Dobrev, Sahithya Phani Babu Vemulapalli, Nilamoni Nath, Christian Griesinger, Gerrit Groenhof, and Helmut Grubmüller*

[https://pubs.acs.org/doi/full/10.1021/acs.jctc.9b01232](https://pubs.acs.org/doi/full/10.1021/acs.jctc.9b01232)

## Charge-Neutral Constant pH Molecular Dynamics Simulations Using a Parsimonious Proton Buffer

Authors: Serena Donnini, R. Thomas Ullmann, Gerrit Groenhof*, and Helmut Grubmüller*

[https://pubs.acs.org/doi/full/10.1021/acs.jctc.5b01160](https://pubs.acs.org/doi/full/10.1021/acs.jctc.5b01160)

# Other publications on constant pH

# Other publications
