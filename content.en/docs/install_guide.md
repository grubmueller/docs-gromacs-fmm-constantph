---
title: "Installation Guide"
date: 2024-01-12T00:00:01+01:00
---

<div class="maintitle">Installation Guide</div>

Our implementation is based on but independent from the upstream [GROMACS](https://www.gromacs.org/) MD simulation code.
It therefore needs to be built separately, creating a Fast Multipole Method (FMM) and constant pH-enabled `gmx` binary that can be used to prepare and run MD simulations.


# Requirements

Our code requires an NVIDIA GPU for simulations, specifically for `mdrun`, due to its reliance on CUDA.
Nevertheless, such a GPU is not required for system preparation steps (e.g., `pdb2gmx`, `grompp`, etc.).
The CUDA toolkit (version 11 or later) must be installed on the machine where the following building steps will be executed.

As MPI parallelization is not currently supported, all builds are limited to single-node simulations, restricting computations to a single machine.

Your installation may require separate builds for your local machine and cluster environment. 
For optimal cluster build configurations, consult your compute infrastructure providers.


# Obtaining the source code

Clone the FMM repository from MPCDF GitLab:

```bash
git clone https://gitlab.mpcdf.mpg.de/grubmueller/fmm.git
```

Move to the `fmm` directory, and switch to the constant pH simulation branch:

```bash
git checkout constant_ph_stable
```


# Building the Code

The following instructions assume `$CPHSOURCEDIR` represents the directory containing the source code. If not already set, you can define it as follows:

```bash
export CPHSOURCEDIR=/directory/with/the/source/
```

Create a separate directory for building the code:

```bash
mkdir build
cd build
```

Configure the build using CMake:

```bash {linenos=false,hl_lines=[3,4,"6-7"],linenostart=1}
cmake -DCMAKE_BUILD_TYPE=Release \
      -DGMX_BUILD_OWN_FFTW=ON \
      -DGMX_GPU=CUDA \
      -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-11.1 \
      -DGMX_SIMD=AUTO \
      -DGMX_WITH_FMM=ON \
      -DGMX_CONSTANTPH=ON \
      $CPHSOURCEDIR
```

The exact configuration is up to your preference, within the following constraints:

- The build must not be a MPI build (do not set `-DGMX_MPI=ON`)
- If building for a different machine, replace `-DGMX_SIMD=AUTO` with the appropriate SIMD level for the target machine
- Enable GPU support with CUDA (set `-DGMX_GPU=CUDA` and necessary follow-up options like `-DCUDA_TOOLKIT_ROOT_DIR`)
- Double precision is optional (set `-DGMX_DOUBLE=ON` if needed, but it's typically unnecessary for most simulations)
- Both FMM and constant pH must be enabled  (set `-DGMX_WITH_FMM=ON -DGMX_CONSTANTPH=ON`)
- For guidance on other parameters, consult the [Gromacs installation guide](https://manual.gromacs.org/documentation/current/install-guide/index.html)


## Troubleshooting

If you encounter problems at this stage, particularly CUDA-related errors, consider the following:

- CUDA toolkit version verified to work:
    - CUDA toolkit 11.1
    - CUDA toolkit 12.2
- Other versions will likely also be able to build this code, but you might want to use one of these if you encounter errors in CMake or compilation not listed below.
- Adding `-DCUDA_NVCC_FLAGS="-Xcompiler;-fPIC"` to your CMake command may resolve some issues (e.g. unresolved symbols or disallowed relocation types)
- If encountering error with `CMakeDetermineCUDACompiler.cmake` or `Failed to detect a default CUDA architecture.`, point to the `nvcc` compiler using `-DCMAKE_CUDA_COMPILER=/usr/local/cuda-11.1/bin/nvcc`
- If there is a incompatibility of your gcc version with CUDA ([see here for a table](https://stackoverflow.com/questions/6622454/cuda-incompatible-with-my-gcc-version)), you might have to install a compatible version, and set `-DCMAKE_C_COMPILER=/home/ebriand/local/gcc-9/bin/gcc` and `-DCMAKE_CXX_COMPILER=/home/ebriand/local/gcc-9/bin/g++` to point toward the correct compiler. We have verified GCC 9 to work, but likely any compiler that would work for normal GROMACS with CUDA would also work here.
- When troubleshooting issues, it is recommended to delete the content of the build directory after a (failed) `cmake` invokation to avoid issues with lingering temporary files and variable values.
- For persistent CUDA problems, consult your local CUDA experts
- If you encounter CMake-related errors:
    - Try CMake version 3.18.4 (verified to work)
    - Note that GROMACS build scripts use relatively recent CMake features, so older versions (common in cluster environments) may not suffice
    - Download specific CMake versions as standalone binaries from [https://cmake.org/files/](https://cmake.org/files/)


# Compilation

After cmake is successful, compile the `gmx` executable (`-j8`: use 8 threads for build):

```bash
make -j8 gmx
```

A successful build will end with output similar to:

```bash {linenos=false,style="solarized-light"}
[100%] Linking CXX executable ../../bin/gmx
[100%] Built target gmx
```

This output indicates that the compilation process has completed successfully and the `gmx` executable has been created.

The `gmx` binary is located in the `bin` subdirectory of your current build directory (i.e. `./bin/gmx`).


# Using the code

Given the path to the build directory, the simplest way to use the code is with an alias:

```bash
alias gmx=/directory/where/gromacs-constantph-installed/bin/gmx
```

or by sourcing the `GMXRC` script:

```bash
source /directory/where/gromacs-constantph-installed/bin/GMXRC
```

You can verify that you have the correct `gmx` binary in your PATH with the following command:

```bash
gmx pdb2gmx -h | grep ldhis
```

which should return something like this:

```bash {linenos=false,style="solarized-light"}
       :-) GROMACS - gmx pdb2gmx, 2024-dev-20240108-249226c0e2-local (-:

Executable:   /home/ebriand/Projects/gromacs_juelich/cmake-build-relwithdebinfo_localsettings/bin/gmx
Data prefix:  /home/ebriand/Projects/gromacs_juelich (source tree)
Working dir:  /home/ebriand/Projects/__CPH_Runs/SimpleLambdadyn/structure_qihi/single_glu_qi
Command line:
  gmx pdb2gmx -h

            [-[no]ldglu] [-[no]ldhis] [-[no]ldasp] [-[no]ldlys] [-[no]ldtyr]

GROMACS reminds you: "Life in the streets is not easy" (Marky Mark)

 -[no]ldhis                 (no)

```

The `-ldhis` option, being specific to the constant pH build, attests that the correct `gmx` binary has been executed.

To get started on actual projects, you may then move on to:
- Download [a constant pH-enabled force field]({{< relref "/docs/cph/forcefields" >}})
- And follow one of our [**Tutorials**]({{< relref "/docs/tutorials" >}}).




