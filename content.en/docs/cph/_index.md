---
title: "Articles"
date: 2017-03-02T12:00:00-05:00
---


<div class="maintitle">Constant pH MD</div>


Constant pH molecular dynamics (CPH-MD) is a variant of molecular dynamics with


See the following to get started:

- [Intro to CPH]({{< relref "/docs/cph/intro-cph" >}}) for general concepts
- [Forcefields]({{< relref "/docs/cph/forcefields" >}}) to download force field files for constant pH simulations
- [Scripts]({{< relref "/docs/cph/scripts" >}}) to automate your CPH simulations (titrations, ...)


After running a constant pH simulation, you will likely want to [analyze its results]({{< relref "/docs/cph-postprocessing" >}}).

You might also be interested in the following tutorials:

- [Constant pH Lyzozyme tutorial]({{< relref "/docs/tutorials/cph-lysozyme-tuto" >}})
- [Constant pH Titration tutorial]({{< relref "/docs/tutorials/cph-titration" >}})

If you want to go beyond the simplest usecase, see:

- [Adding a custom protonatable residue]({{< relref "/docs/cph/custom-res" >}})
- [Calibration]({{< relref "/docs/cph/custom-res" >}}) on general information to calibrate a new forcefield for constant pH runs
- [Polynomial fit to calibration data]({{< relref "/docs/cph/polynomial_fit_calibration_data" >}}) on how to do the final step of polynomial fit to the calibration data

Implementation details and file format:

- [Constant pH `.dat` output format]({{< relref "/docs/cph/raw_dat_output_format" >}})

