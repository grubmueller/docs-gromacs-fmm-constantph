---
date: 2017-04-09T01:00:00-00:00
description: "Box size and buffer distance discussion"
tags: ["cph", "box size", "buffer distance"]
title: "Box size and buffer distance effects"
weight: 100
---

<div class="maintitle">Box size and buffer distance effects</div>

<div class="box_note">
<div class="box_note_heading">Website under construction</div>
<div class="box_note_text">
Please note that this page is currently a work in progress.

</div>
</div>

Multiple effects exist that can potentially shift residue \\( \mathrm{p}K_\mathrm{a} \\) constant pH simulations. 

Some of these effects are linked to specific implementation decisions, while other are generic and due to the properties of electrostatics in periodic boundary conditions. In particular, the former will differentially affect PME and FMM-based implementation, whereas the latter are universal.


# How to avoid these effects?

For practical purpose, the following recommendation should limit the potential exposure of your simulations to such effects:

- Simulate at box size equal or larger than the calibration box size for your force field files. Typically 6x6x6 nm, see [our force fields page]({{< relref "/docs/cph/forcefields" >}}) for specifications.
- Simulate with buffer at the calibration distance. Typically 3 nm, see [our force fields page]({{< relref "/docs/cph/forcefields" >}}) for specifications).


It is also possible that a finite size correction might have to be applied, if the solvent number density (number of solvent molecule divided by volume of the box) deviate markedly from the calibration density (32.3056 water molecule/nm^-3). See the paragraph "Finite-Size Corrections to the Calculated pKa Values for Proteins" in [(Harris 2022)](https://pubs.acs.org/doi/full/10.1021/acs.jctc.2c00586).
We currently have conflicting empirical evidence on whether that is necessary with Hamiltonian-interpolation FMM constant pH, and are investigating further.





