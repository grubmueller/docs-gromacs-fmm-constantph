---
date: 2017-04-09T02:00:00-00:00
description: "An introduction to constant pH molecular dynamics"
featured_image: ""
tags: []
title: "Intro to constant pH MD"
weight: 100
---


<div class="maintitle">Introduction to constant pH MD</div>

<div class="box_note">
<div class="box_note_heading">Website under construction</div>
<div class="box_note_text">
Please note that this introduction page is currently a work in progress.
</div>
</div>

Constant pH Molecular Dynamics (CPH-MD) is the umbrella term for a variety of techniques in which Molecular Dynamics (MD) simulations are augmented with runtime changes in the protonation state of molecules to simulate at a well-defined pH. Like temperature and pressure, pH is an important condition that must be modelled correctly in order to obtain more accurate simulations.


We will first explain the benefits of constant pH methods for Molecular Dynamics (MD) in general, then focus on our particular constant pH implementation of Hamiltonian interpolation $\lambda$-dynamics.


# Background


## Conformation and protonation

A protein is a polymer of amino acids (also called residues) that folds into a 3-dimensional structure. 
Some of the residues are protonatable, such as histidine or glutamic acid: they have a protonated form and a deprotonated form.

<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/prot_frag_his_deprot.png" "general_figure_image" "Deprotonated histidine sidechain" "45%" "inline" >}}
    {{< baseimg1 "/images/prot_frag_his_prot.png" "general_figure_image" "Protonated histidine sidechain" "45%" "inline" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Protonated and deprotonated form of histidine</span>
    (only sidechain imidazole ring is shown)
    </figcaption>
</figure>

The starkest difference between the protonated and deprotonated forms of a residue is the formal charge of the side chain, from neutral to $\pm 1e$ charge. This causes the two forms to have different electrostatic interactions with the rest of the protein, affecting its dynamics.
Conversely, the dynamics affect the local environment of the residues, and therefore their protonation state.
There is a feedback between dynamics and protonation: correctly modelling this interplay to produce more accurate conformational ensemble in simulation is the goal of constant pH MD.

In particular, the conformation of the ...

<div class="box_note">
<div class="box_note_heading">Interactive figures</div>
<div class="box_note_text">The following figure is interactive: you can click-and-slide the yellow circle to move the alpha helix. Figures with similar interactivity will have the same sort of yellow circle or yellow rectangle interactive controls.</div>
</div>

<figure class="general_figure" style="width: 80%">
    {{< iframeviz1 "viz/cph_explain_env.html" 430 >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Conformation and protonation</span>
    : This cartoon example shows the influence of nearby electrostatic charges (negative charges on the alpha helix) on the protonation state of the glutamic sidechain. The p$K_a$ of the glutamic acid residue (4.4) is such that, in absence of other influences, it is overwhelmingly likely to be found in the deprotonated state at physiological pH (7.4). The negative charges on the helix are such an influence: they can shift the p$K_a$ (i.e., the free energy of protonation for the residue). If strong enough, this effect can make the protonated form the most favorable at the current pH.
    </figcaption>
</figure>



## Non-constant pH MD

However, in non-constant pH MD

<figure class="general_figure" style="width: 80%">
    {{< iframeviz1 "viz/noncph_explain_env.html" 430 >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Non-constant pH MD</span>
    : Similar cartoon example, without dynamic protonation. The electrostatic repulsions make certain conformation very energetically unfavorable, as they would need the glutamic acid residue to be protonated.
    </figcaption>
</figure>

Therefore, the conformational ensemble of normal, non-constant pH MD is likely less representative of the actual ensemble as constant pH MD, all other factors equal (force fields accuracy, sampling, ...)

## Why constant pH MD

- More correct protonation state
- More accurate conformational ensemble
- Simulation at different pH: titration

### Advanced protonation study through constant pH MD

Beyond more accurate MD, constant pH technique also allow characterizing the behavior of the protonatable residues, for instance obtaining p$K_a$ by virtual titration; or studying the coupling between the protonation state of multiple residues.

<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/cardiotoxin_fma_struct_solventexposed_FMA_low.png" "general_figure_image" "Conformation of cardiotoxin favorable to the protonated state" "45%" "inline" >}}
    {{< baseimg1 "/images/cardiotoxin_fma_struct_solventNotExposed_FMA_high.png" "general_figure_image" "Conformation of cardiotoxin favorable to the deprotonated state" "45%" "inline" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Example of investigation made possible by constant pH</span>
    : In this [snake venom protein ](https://www.rcsb.org/structure/1cvo), the pKa of the His 4 residue shifts from 4.5 to 3.5 between these two conformation.
    This is an example of <a href="/docs/cph-postprocessing/fma">Functional Mode Analysis</a> applied to protonation.
    </figcaption>
</figure>

<p>&nbsp;</p>

<figure class="general_figure" style="width: 50%">
    {{< baseimg1 "/images/cardiotoxin_asp42_fma_extremal_interactions_tr.png" "general_figure_image" "3D model of the cardiotoxin V as cartoon, with the residue involved in the interaction seen as all-atom wire" "90%" "inline" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Example of investigation made possible by constant pH</span>
    : The protonatable residue of interest, Asp 42 in the snake toxin Cardiotoxin V, was found to have a different p$K_a$ in two conformations of the protein, due to different interactions, either ionic with positively-charged Lys 19, or hydrogen bonding with Gly 18.
    </figcaption>
</figure>



## "Perfect" constant pH: QM and explicit protons

In the previous cartoon example, we only considered the difference in p$K_a$ (i.e., free energy of (de)protonation), which are thermodynamic quantities. We considered the change in protonation instant, and without the presence of explicit protons ($H^{+}$), and have therefore completly neglected the kinetics of the reaction.

This is a simplification of the process: in actuality, the change of protonation state is linked with a chemical reaction involving bond breaking or formation between the hydrogen ion ($H^{+}$), the residue, and water molecules ([hydronium](https://en.wikipedia.org/wiki/Hydronium), Eigen and Zundel cation ...). As with any chemical reaction, the themodynamics and kinetics of the reaction linkled

This proton then diffuses, with [a mechanism](https://en.wikipedia.org/wiki/Grotthuss_mechanism) which again involves bond breaking and formation.


Therefore, we could strive to model this process as accurately as possible, by treating 

However, such approach would be extremely slow


## Sampling-accuracy trade-offs

The majority of constant pH methods do not attempt to perfectly reproduce the chemistry and physics involved in the protonation mechanism.
Rather, they approximate part of it, to allow.


# $\lambda$-dynamics

$\lambda$-dynamics is one such trade-off methods, giving up the kinetics of the protonation reaction in favor of sampling speed, while trying to keep intact the thermodynamics (therefore, p$K_a$s) of the process.





All of those element 

<figure class="general_figure" style="width: 100%">
    {{< iframeviz1 "viz/lambda_explain.html" 420 >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Constant pH $\lambda$-dynamics</span>
    : You can interact with the yellow slider to simulate protein motion. The $\lambda_p$ protonation coordinate responds to the local environment, driven by the force $F_{H_{interp}}$, which is the difference in electrostatic energy between the protonated and deprotonated states. This is how constant pH $\lambda$-dynamics achieves correct protonation. The green slider directly controls $\lambda_p$ (for illustration purpose: there is no such control in the actual simulation). The $\lambda_p$ pseudoparticle is evolves in a double-well potential (right), which minimises the time spent in the unphysical intermediate states. Finally, the velocity of the pseudoparticle is coupled to a thermal bath, to yield a canonical (isothermal) simulation, as shown in the thermal noise of the $\lambda$ trajectory trace.
    </figcaption>
</figure>


Output:

<figure class="general_figure" style="width: 60%">
    {{< baseimg1 "/images/only_lambda_graphs.png" "general_figure_image" "Small excerpt of a lambda trajectories, for the protonation and tautomer coordinate. The noise of the trajectory is prominent." "90%" "inline" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Excerpt of $\lambda$ trajectory</span>
    : for both the protonation and tautomeric coordinate. The 
    </figcaption>
</figure>



# Site and forms



# Protonatable residues

# How realistic is CPH-MD?

- [ ] Why CPH-MD versus QMMM?
- [ ] Electrostatic-only versus general sites


# Titration

