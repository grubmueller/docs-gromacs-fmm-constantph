---
date: 2024-02-05T00:00:01-00:00
description: "Calibration Python Script: How to Use"
featured_image: ""
tags: []
title: "Calibration Python Script: How to Use"
weight: 100
---

# Calibration Python Script: How to Use

There is a script available to automate running simulation to calibrate a residue, at a given distance.

This script will not do the forcefield files modifications for this new residue - see 

[Adding a custom protonatable residue]({{< relref "/docs/cph/custom-res" >}})

## Titration


## Calibration


## Misc snippets


### FMA
