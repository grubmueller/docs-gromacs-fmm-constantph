---
date: 2024-01-15T00:00:01-00:00
description: "Output format of the .dat constant pH trajectories"
featured_image: ""
tags: []
title: "Output format of the .dat constant pH trajectories"
weight: 100
---

<div class="maintitle">Output format of the .dat constant pH trajectories</div>

The `.dat` output trajectory are simple space separated text files, with one line per residue (or buffer site) containing the state of that residue at the given timestep.

Comment lines start with `;`, and contain information that is not directly the trajectory, like the settings for the run, for diagnostic or traceability purpose. Those are emitted at the beginning and end of the simulation, as well as during for the dynamic barrier and well adjustment.

One of the comment, near the beginning of the trajectory itself, documents the order of the space-separated fields. We reserve the right to change the fields, and their order, in subsequent version, though in practice the format is relatively stable.


```
; [ time ] [ RorB ] [ site ] [ l_p ] [ l_t ] [ v_p ] [ v_t ] [ f_p ] [ f_t ] [ dhdl_p ] [ dhdl_t ] [ ffcomp_p ] [ ffcomp_t ] [ bias_p ] [ bias_t ] [ constr_p ] [ constr_buff ] [ f_buff ] [ therm ] [ inhib ] 
; Restarting from checkpoint
; Starting internal state is:
;     residue [   0]  lambda_p =         1.0757195950   velocity =         -0.0104046911  last_force =      -205.1779327393
;                     lambda_t =         0.7902489305   velocity =          0.0634180531 last_force =         9.1901779175
; Reading buffer checkpoint:
;      collective buffer lambda         0.0000000000 (v:         0.0000000000  F_(t-1)         0.0000000000)
      0.000  r   0 1.075687 0.790344 -0.012844 0.047467 -205.095459 9.176412 107.917526 57.379688 212.144714 56.451721 -100.868263 8.248445 0.000000 0.000000 0.000000   0   0
      0.000  b   0 -0.075687 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 823710461516290818310144.000000 0.000000 0.000000 0.000000 0.000000   0   0

```

# Field documentations

The field shorthand corresponds to the following data. The most relevant fields are in bold: the others are mostly for method developement, and not necessarily relevant to titration or typical simulation applications.


| Shorthand          | Meaning                                                          |
|--------------------|-----------------------------------------------------------------------|
| **`[ time ] `**    | Time (ps) in the trajectory                                           |
| **`[ RorB ]`**     | Whether the line is about a residue `r` or a buffer `b` (counter-ion) site |
| **`[ site ]`**     | Site index (0-based)                                                  |
| **`[ l_p ]`**      | $\lambda_p$, the protonation $\lambda$ coordinate                     |
| `[ l_t ]`          | $\lambda_t$, the tautomer $\lambda$ coordinate                        |
| `[ v_p ]`          | Velocity of the $\lambda_p$ coordinate pseudoparticle                 |
| `[ v_t ]`          | Velocity of the $\lambda_t$ coordinate pseudoparticle                 |
| `[ f_p ]`          | Total force on the $\lambda_p$ coordinate pseudoparticle              |
| `[ f_t ]`          | Total force on the $\lambda_t$ coordinate pseudoparticle              |
| `[ dhdl_p ]`       | $\partial{\mathcal{H}}/\partial{\lambda_p}$  (most important force subcomponent)                          |
| `[ dhdl_t ]`       | $\partial{\mathcal{H}}/\partial{\lambda_t}$                           |
| `[ ffcomp_p ]`     | Calibration force subcomponent for  $\partial{\mathcal{H}}/\partial{\lambda_p}$  |
| `[ ffcomp_t ]`     | Calibration force subcomponent for  $\partial{\mathcal{H}}/\partial{\lambda_t}$  |
| `[ bias_p ]`       | Force component from double well potential ("bias potential") for $\lambda_p$ |
| `[ bias_t ]`       | Force component from double well potential ("bias potential") for $\lambda_t$ |
| `[ constr_p ]`     | Charge constraint force                                              |
| `[ constr_buff ]`  | Charge constraint force (buffer-related)                             |
| `[ f_buff ]`       | Force on buffer coordinate                                           |
| `[ therm ]`        | Whether a thermostat event has occured (velocity exchange)           |
| **`[ inhib ]`**    | Whether the frame is censored (should be discarded from analysis)    |




## Notes on the fields

## $\lambda_p$  convention

 By default, $\lambda_p$ is defined such that a value of `0` is deprotonated and `1` is protonated. See also the [lambda output convention and the associated MDP parameter]({{< relref "/docs/cph/cph_parameters#lambda-convention" >}}).
 
 In the internal convention, which is not activated by default, and can be obtained by setting `constantph-output-lambda-as-protonation = no`, the form without tautomer is $\lambda_p=0$, and the one with two tautomer is $\lambda_p=1$. For Glu, this leads to $\lambda_p=0$ for the deprotonated form, while for his $\lambda_p=0$ is the double protonated form.

 
If using the dedicated [CPH post-processing pipeline]({{< relref "/docs/cph-postprocessing" >}}), this is automatically taken into account; however custom scripts should take notice.
 
## $\lambda_t$  convention for Histidine
 
The two tautomer for Histidine are not equivalent chemically. The $\delta$ tautomer, with an hydrogen (HD1) on ND1, and none of NE1, corresponds to $\lambda_t=0$. The $\varepsilon$ tautomer, with an hydrogen (HE1) on NE1 is thus present when $\lambda_t=1$.

## Frame censoring

Frames are censored (field `[ inhib ]` set to 1) for a short duration after the double well potential was adjusted (dynamic well/barrier adjustment) to eliminate the influence of the instantaneous potential change. This is also taken into account by the dedicated tool.

## Thermostat event

Thermostat events are relevant to the Andersen thermostat, and correspond to a velocity exchange with a randomly drawn one from the Boltzmann distribution. Thye number of exchanged velocity is printed in this column, up to a maximum corresponding to the total number of protonatable residue. The `v-rescale` thermostat is continuous (velocity continuously rescaled), so this column is uniformly zero.

## Total force

The total force is the sum of the `dhdl`, the `ffcomp`, the `bias` and the `constr` subcomponent. The absolute value of the `dhdl` and `ffcomp` are calibrated to be equal for a single residue in water (calibration process).



