---
date: 2017-04-09T01:00:00-00:00
description: "Constant pH Forcefield files"
tags: ["cph", "forcefield"]
title: "Constant pH Forcefield files"
weight: 100
---


<div class="maintitle">Constant pH Forcefield files</div>


This page contains download links for the constant pH modified forcefields.

# Charmm36m with constant pH modifications

Based on J. Lemkul port of charmm36m, with support for Glu, Asp, His, and for buffer site at a distance of **3.0 nm**. The minimum box size supported is 6x6x6 nm for this calibration ([See also our discussion of box size and buffer distance effects]({{< relref "/docs/cph/box_size_buffer_distance" >}})).

[charmm36m-lambdadyn.ff.tar.gz tarball](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/charmm36m-lambdadyn.ff.tar.gz) (v1.0)


**Please cite**:

[CHARMM36m: an improved force field for folded and intrinsically disordered proteins](https://www.nature.com/articles/nmeth.4067)

## Unvalidated uses

### Additional residues

Tyr and Lys are also included as protonatable residues in this force field package (usable through `-ldlys`, `-ldtyr`).
We only validated these residues in single-residue titrations (self-consistency check), not against measured pKa values in proteins.
Therefore, we cannot fully endorse their use, and recommend them only for exploratory or preliminary work with critical examination of the obtained results.

### Preliminary NPT equilibration

We have validated our constant pH code, and these force field files, for simulations that ran exclusively in the NVT ensemble, i.e. with solvation using `gmx solvate` followed by equilibration and production simulations taking place at constant volume, as our code does not yet support pressure coupling.

It is also possible to run a short NPT equilibration, without constant pH, to equilibrate the unit cell size, before proceeding with the NVT constant pH MD simulations.
Given that we plan on implementating pressure coupling, which would render this procedure moot, we did not extensively test this use case, and therefore *do not recommend it*.
Users nonetheless wishing to use this procedure should add the following to their MDP file for constant pH runs:

```scheme {linenos=false,style="monokailight"}
define                  =  -DCPH_PRESSUREEQUIL_FIT
```

Of note, we have investigated the effect of such NPT pre-equilibration on titration of the lysozyme protein (HEWL), as shown in the figure below, with minimal apparent effect on residue pKa.

<figure class="general_figure">
    {{< baseimg1 "/images/lysozyme_pressure_equil_vs_nvt.png" "general_figure_image" "scatter plot of pKa with and without NPT equilibration for the lysozyme protein" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Comparison of pKa values with and without a pre-constant pH NPT equilibration step</span>, for the lysozyme (HEWL) system.
    </figcaption>
</figure>





## Older versions

The current version is the first public release.


# Amber99sb*-ILDN with constant pH modifications

With support for Glu, Asp, His, and for buffer site at a distance of **3.0 nm**. The minimum box size supported is 6x6x6 nm for this calibration ([See also our discussion of box size and buffer distance effects]({{< relref "/docs/cph/box_size_buffer_distance" >}})).

[amber99sb-star-ildn-lambdadyn.ff.tar.gz tarball](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/amber99sb-star-ildn-lambdadyn.ff.tar.gz) (v1.0)


**Please cite**:

[Optimized Molecular Dynamics Force Fields Applied to the Helix−Coil Transition of Polypeptides](https://pubs.acs.org/doi/full/10.1021/jp901540t)

[Improved side-chain torsion potentials for the Amber ff99SB protein force field](https://onlinelibrary.wiley.com/doi/full/10.1002/prot.22711)



## Older versions

The current version is the first public release.

# Calibration data for specific applications

In this section, we provide individual sets of calibration data collected for various molecules and projects.
These do not make up a full force field distribution, but might save you some calibration work if they happen to
fit your needs.

If you are mixing these with existing forcefield data, you might want to rename the sets (the name have no special meaning, they should just match with `ffcomp_tl_from_common`, see [Topology Format]({{< relref "/docs/cph/lambda_cph_topology" >}})). This is because ffcomp sets with the **same name** can overwrite each other, therefore cause potentially difficult to debug problems linked to order of apparition in the topology file.


## charmm36m, distance 5 nm, Histidine

```scheme {linenos=false,style="monokailight"}
; Calibrated 2023-02-12 - Regularized Regression - COUPLED residue-buffer  DISTANCE d = 5 nm (****!!!*****)
; PROT degree = 10 rdeg =  5 | alpha = 0.000000024 | train  MSE     0.715834 R^2 0.9999977270
; TAUT degree = 10 rdeg =  5 | alpha = 0.000000010 | train  MSE     0.306410 R^2 0.9999627834
ffcomp_coeff_tl_common HISL_QOnly_c p 0   -348.9202618   1433.0177564     90.3096459  -1680.9013113   2277.9171958   -829.6814755    
ffcomp_coeff_tl_common HISL_QOnly_c p 1     65.0550062   -255.1128759    -23.8558612   1546.2600807  -3205.7310504   1434.3718855    
ffcomp_coeff_tl_common HISL_QOnly_c p 2      2.0089929    286.9097916   -504.9819821   -992.8507147   4760.4622008  -2845.2718962    
ffcomp_coeff_tl_common HISL_QOnly_c p 3     10.2370952    116.9419968   -127.0979437    615.4019899  -2958.3759801   1953.7421967    
ffcomp_coeff_tl_common HISL_QOnly_c p 4    -18.6379021   -331.3092053   1097.8966895    168.1166757  -2809.0743886   2024.5152080    
ffcomp_coeff_tl_common HISL_QOnly_c p 5      7.4614587    181.7084647   -453.7907494  -1258.8286264   4049.4867149  -2477.4129395    
ffcomp_coeff_tl_common HISL_QOnly_c t 0      0.3684845     63.0641849   -170.6741136    300.7476969   -332.9699720     52.9426555  
ffcomp_coeff_tl_common HISL_QOnly_c t 1     -1.6131021     15.2784080    478.5160703  -1609.6198029   2523.3702781   -991.6030167  
ffcomp_coeff_tl_common HISL_QOnly_c t 2     -1.6407208      4.1481037    118.4677250   1118.9738804  -4071.4783609   2637.1816622  
ffcomp_coeff_tl_common HISL_QOnly_c t 3     13.6056034    -76.7314004  -1468.9802374   3337.8276175   1205.4316137  -3328.2658594  
ffcomp_coeff_tl_common HISL_QOnly_c t 4    -16.0317222     86.3423393   1601.0500224  -3510.0819654  -1635.2237671   4106.6952674  
ffcomp_coeff_tl_common HISL_QOnly_c t 5      4.9451145    -23.6844944   -388.3631159    103.6023602   2538.5541710  -2471.3596154  
```

## charmm36m, distance 5 nm, Glutamic acid, protonation only

This set of data only covers protonation - it can be used with a modified glutamic acid whose two tautomer are identical (no support for tautomery).

```scheme {linenos=false,style="monokailight"}
; Calibrated 2023-02-12 - Regularized Regression - COUPLED residue-buffer  DISTANCE d = 5 nm (****!!!*****)
; PROT degree =  5 rdeg =  5 | alpha = 0.000002982 | train  MSE     0.264887 R^2 0.9999992971
; TAUT N/A
ffcomp_coeff_tl_common LBH_QOnly_c p 0   -1245.19736537  1609.19389821  -670.51318522    -4.10304996  987.60585364  -537.42865 
```
