---
date: 2024-02-16T00:00:01+01:00
description: "Constant pH MDP parameters"
tags: ["cph"]
title: "Constant pH MDP parameter"
weight: 100
---


<div class="maintitle">Constant pH MDP Parameters</div>

We describe here the parameters, available in GROMACS MDP files, to configure the constant pH feature of our code.

# Constant pH simulation

The constant pH code can be enabled with the following MDP parameters:

```scheme {linenos=false,style="monokailight"}
constantph-enable        = true    ; Constant pH code itself 
lambda-updater-enable    = true    ; Provides the dHdl values for the CPH code
```

## Mode of operation

Different modes of operation are available for the constant pH module, selectable using the `constantph-mode` parameter:

```scheme {linenos=false,style="monokailight"}
constantph-mode           = dynamics
```

The modes are as follow:

- `genion`: to be used in the `mdp`/`tpr` file for genion (in conjunction with `constantph-enable = yes`), so that the number of buffer site is automatically inferred
- `dynamics`: constant pH MD proper, using $\lambda$-dynamics.
- `ti`: thermodynamic integration mode, to be used for calibration of new protonatable residues



## pH setting

In the $\lambda$-dynamics. mode of operation, the pH is set as follow:

```scheme {linenos=false,style="monokailight"}
; pH for run
constantph-ph-mode        = fixed
constantph-fixed-ph       = 6.38
```

The `constantph-ph-mode` is set to `fixed` to simulate at the specific pH given in `constantph-fixed-ph`. Since the pH value internally sets the well depth of double well potential for each residue, it can be set to any value, including outside of the 0-14 interval. While extreme pH conditions might not be realizable experimentally, such pH might be useful to obtain titration curve for residue with very high or low pKa, or to study acidic or basic protein denaturation processes.


## Integrator timestep

The integrator timestep for the MD simulation is set using `dt`. The $\lambda$ integrator should have the same timestep. If  the timestep for the simulation is different from 2 fs (0.002 ps), then it can be manually set with:

```scheme {linenos=false,style="monokailight"}
constantph-integrator-timestep = 0.002
```

# Thermostat

The $\lambda$ subsystem is kept at a constant temperature using a thermostat that acts on the velocity of the $\lambda$ pseudoparticle. The default parameters for the thermostat are as follow:

```scheme {linenos=false,style="monokailight"}
; Lambda subsystem thermostat
constantph-tcoupl         = v-rescale
constantph-tcoupl-tau     = 0.1   ; ps
constantph-tcoupl-target  = 300 ; K
```

The following thermostats are available:

- `v-rescale` is the Bussi-Donadio-Parnillo thermostat ([Canonical sampling through velocity-rescaling](https://arxiv.org/abs/0803.4060)). This is the recommended thermostat.
- `andersen` is the [Andersen Thermostat](https://en.wikipedia.org/wiki/Andersen_thermostat).

The coupling constant for the thermostat is `constantph-tcoupl-tau`, and the temperature with `constantph-tcoupl-target`, similarly to the MD simulation.


Both thermostats correctly sample the canonical ensemble. The `v-rescale` thermostat has two advantages: it is very insensitive to the choice of coupling constant timescale (from 0.1 to 10.0 ps, see the [publication](https://arxiv.org/abs/0803.4060) for details), and does not produce velocity discontinuity. It is not necessary for the thermostat of MD and the $\lambda$ subsystem to match, though the temperature set point should be the same. 



# Buffer sites



## Buffer site keep-away potentials

To keep the buffer sites in the bulk solvent, preventing interaction with the titratable residues, the protein and the other buffer sites, one-sided distance restraint, or "keep-away" potential are used.

There is a general setting for the distance restraint, which can be overriden with more specific directives:

```scheme {linenos=false,style="monokailight"}
constantph-buffer-site-keepaway-distance = 3 ; nm
```

Distance restraint are harmonic potentials, whose force constant can be set for all of them (kJ/mol):

```scheme {linenos=false,style="monokailight"}
constantph-buffer-site-keepaway-force-constant = 50  ; kJ/mol
```

The first of the aforementioned more specific directives maintains the distance between the titratable residue and the buffer site to a fixed value, and to make it two sided (not only is the minimal distance kept, but the buffer site will not drift away more than 3 nm too, as required in current iterations of the constant pH code):

```scheme {linenos=false,style="monokailight"}
constantph-buffer-site-keepaway-to-residue-distance = 3    ; nm
constantph-buffer-site-keepaway-to-residue-two-sided = true
```

The distance restraint involving the protein do not act on all atoms, but only on a select group, for computational efficiency reasons. This set of atom can be chosen by an index group:


```scheme {linenos=false,style="monokailight"}
constantph-buffer-site-keepaway-group = C-alpha
```

This sets the distance restraint between, for instance, the buffer and its titratable residue, to act between the oxygen atom of the water molecule comprising the buffer, and the `C-alpha` atom of the titratable residues. For restraint between buffer and protein in general, there is one restraint per `C-alpha` in the protein (for each buffer separately).

The distance to the protein in general can be set independantly from the distance to the titratable residue (this of course need to be smaller than `constantph-buffer-site-keepaway-to-residue-distance`):

```scheme {linenos=false,style="monokailight"}
constantph-buffer-site-keepaway-to-protein-distance = 2    ; nm
```

Similarly, the distance to other buffer site can also be set:

```scheme {linenos=false,style="monokailight"}
constantph-buffer-site-keepaway-to-buffer-distance = 2    ; nm
```

If none of the more specific distance directive are set, all are assigned the value of `constantph-buffer-site-keepaway-distance`.


# Outputs

## $\lambda$ convention

By default, the $\lambda$ values reported in the output file (`.dat`) are in the *internal convention*, where $\lambda_p = 0$ always correspond to the form of the site without tautomers, whereas $\lambda_p = 1$ is the form where $\lambda_t$ switches between the different tautomers. This does not map for all residue into the *usual convention*, namely that $\lambda_p = 0$  is deprotonated and $\lambda_p = 1$ is protonated.


<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/lambda_p_and_t.svg" "general_figure_image" "Carboxylic acid group in the internal convention of this code" "100%" "block" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Representation of the carboxylic acid moety in the  <i>internal convention</i></span>
    which happen to coincide with the <i>usual convention</i> for this residue, but does not for, e.g., histidine.
    </figcaption>
</figure>

You can recover the *usual convention* with:

```scheme {linenos=false,style="monokailight"}
constantph-output-lambda-as-protonation = yes
```

This is purely for output purpose, none of the internals (nor topology directive like `init_lambda_tl`) are affected by this change.

More informations on the output file formatting can be found [here]({{< relref "/docs/cph/raw_dat_output_format" >}}).

## Output frequency

While the constant pH code is executed every time step, the frequency at which its state is written to the output `.dat` file is controllable (number of MD timestep between output):

```scheme {linenos=false,style="monokailight"}
constantph-nstout        = 500
```


# TI and Calibration-related parameters

TODO. For now, here is an example:

```scheme {linenos=false,style="monokailight"}
constantph-ti-axis = GLUL0002 p BufWater0001 buffer
constantph-ti-delta-lambda = 0.0 0.0
constantph-disabled-buffers = all   
constantph-ti-delay-steps = 0
```

# Barrier and well optimization

TODO!

This code contains an algorithm that optimize the shape of the double well potential to fullfill multiple requirements.


- `constantph-statistics-**-block-duration`: duration in picosecond of the short and long block for statistics purpose. The average lambda value for bias adjustment is collected over the whole trajectory. The ratio of frame spent in transition is collected from the last long block (to reflect recent bias barrier change). Update to the well will occur at the end of each short block, and to the barrier at the end of each long block
- `constantph-*-lambda-frame-in-transition`:  set the lower and upper bound for a frame to be considered in transition (per site, originally 0.2-0.8). The fraction is the target for the `target-protonation-in-transition-fraction` setting.
- `constantph-bias-adjust-mode`: either `original` for how it works usually, or `combined-global-average` for this new bias adjustment. 
- `constantph-online-well-position-adjustment`: wether to adjust the well position during the run. `constantph-online-barrier-height-adjustment`: same for the barrier. 
- `constantph-well-average-position-tolerance`: how much deviation in average lambda value is tolerated before adjustment of the well occurs.
- `constantph-well-position-adjustment-maximum-distance`: the well will not be adjusted beyond (ideal position +/- this value), so for instance if this is 0.1, then the 0 well could be betwene -0.1 to 0.1. If the lambda average is not within tolerance, but the well adjustment is already at this bound, the `overshoot` flag is set for this barrier height (and the minimal barrier height will be higher than )
- `constantph-well-adjust-proportional-factor`: proportional factor in the well adjustment. Eg. lambda error = 0.0 - avg_lambda = 0.3   -> proportional factor 0.5 -> adjust well by 0.3 x 0.5 = 0.15. Value 0.5 works fine.
- `constantph-bias-partfunccorr`: whether partition function correction is applied. This should be `yes` for the original bias mode, not sure for `combined-global-average` mode, but probably also yes.
- `constantph-bias-initial-barrier`: initial height (gromacs energy unit, kj/mol) of the barrier. If barrier adjustment is disabled, this will be the barrier for the whole run.
- `constantph-bias-tautomer-barrier-override`: since mode `combined-global-average` does not adjust the tautomer barrier, this is the height of the tautomer barrier for the whole run.
- `constantph-barrier-adjustment-*-barrier`: absolute minimum and maximum value of the barrier when adjusting the barrier automatically. The above-described `overshoot` mecanism might raise the minimum barrier reachable in practice.
- `constantph-barrier-adjustment-delta-amount`: By how much the barrier is raised or lowered, when it is necessary, at the end of each long block. Was 1 kj/mol in the `original` bias adjust algorithm.
- `constantph-bias-adjust-minimal-additional-time-spent`: when trying to adjust the well position, will only adjust the 0 or 1 well, if at least X ps (this setting value) was spent in the -Inf-0.5 or 0.5-+Inf interval respectively since last time we adjusted the well. The point of this setting is avoiding tweaking, for instance, the 1.0 well when the lambda particle didnt spent any additional time since the last change (could overcorrect if not present)
-  `constantph-bias-adjust-overshoot-count-tolerance`: At the end of each short block, if the average lambda is not correct, but the well adjust is at the maximum limit set, then an `overshoot` condition occurs. The barrier will be flagged as too low, and a new minimum barrier will be set after N overshoot conditions, where N is the value of this parameter. Setting this to value above 1 give a little time (N short block piconseconds) for the average value to normalize before flagging the barrier, which might be necessary if the average value was previously very wrong (see concept of Integral windup). Setting it to zero apply the flagging as soone as any overshoot is detected. As raising the minimum barrier level is permanent, it makes sense to put 1 or 2 here.
- `constantph-target-protonation-in-transition-fraction` : target value the barrier
- `constantph-load-bias-setting-from-tpr-checkpoint`: if set to Yes, the well positions and barriers are loaded from the tpr file, which gets them from the checkpoint file passed to `grompp`. Typically, set this to `no`. However, maybe you want to separate the bias potential tuning and the production run: then set to `no` with bias adjustment enabled (will optimize positions and barriers), then `yes` with bias adjusment disabled for the production run (static bias). In particular, `constantph-bias-initial-barrier` is then ignored in favor of the last height (per site) of the previous run.
- `constantph-post-bias-modification-censoring-duration`: The output dat file contains an inhib flag in the last column, which is either 0 or 1, for each site independantly. Whenever the bias for a site has been adjusted, the inhib flag is set for this number of picosecond - analysis tool might want to ignore those frames due to the instantaneous hamiltonian change. `10` ps seems cautious without discarding too much sampling.





# Advanced topics

The following options are not meant for the typical user of constant pH, but are either facilities for characterizing or debugging the constant pH code, or for method developement. Specifying those settings might render the constant pH simulation incorrect.



## Tautomer freezing

As explained in [Intro to CPH]({{< relref "/docs/cph/intro-cph" >}}), the simulated protonatable residues have tautomeric forms in addition to the protonated/deprotonated forms. If desired, the tautomer axis can be disabled with the following parameter:

```scheme {linenos=false,style="monokailight"}
constantph-freeze-tautomer = yes
```

In that case, the tautomer will keep its initial value, which is set in the [topology]({{< relref "/docs/cph/lambda_cph_topology" >}}) with `init_lambda_tl`.


## Debugging informations

The constant pH code can be run in a debugging mode with the following option:

```{linenos=false,style="monokailight"}
constantph-debug = yes
```

This enables more messages to the standard output, as well . There are some runtime cost to this mode. When `grompp` or `mdrun` is executed, the following files will be created in the present working directory:

- `debug_stats.dat`: Information from the statistics submodule on the average lambda value in the last statistics short block, as well as for the whole runtime, block. 
- `debug_biasadjust.dat`: Information from the well and barrier optimization algorithm, including the step at which changes have taken place, and the reason for those (average lambda, number of transitions ...)
- `debug_biasshape.X.csv`: For each site (with index `X`), the shape of the double well potential at the start of the simulation. (csv format: `lambda,potential`)

### Format of the statistics debug file


```scheme {linenos=true,linenostart=1,hl_lines=[26],style="monokailight"}
; SHORT block finished 
; Site[  0] avg l_p  0.600164 l_t  0.160780
; Site[  0] (TwoIntervals, l < 0.5) avg l_p  0.079953 l_t  0.019363
; Site[  0] (TwoIntervals, l > 0.5) avg l_p  0.910365 l_t  0.771745
;      pole <p 0.0 t 0.0> avg l_p  0.031683 l_t  0.014710 (fr. frame 0.292)
;      pole <p 1.0 t 0.0> avg l_p  0.990441 l_t  0.019817 (fr. frame 0.310)
;      pole <p 0.0 t 1.0> avg l_p      -nan l_t      -nan (fr. frame 0.000)
;      pole <p 1.0 t 1.0> avg l_p  1.005286 l_t  0.921486 (fr. frame 0.076)
0    0.38319    0.80054 TRANS_T 
0    0.19954    1.02887 TRANS_P 
```

New data is added to `debug_stats.dat` at the end of each short and long statistics blocks. First, the average lambda values for the whole run are printed (line 2), as well as for each sub-interval $\lambda <0.5$ and  $\lambda >0.5$ (line 3 and 4). Average lambda in the last short block, when near a combination of $\lambda_p$ and $\lambda_t$ are printed in line 5 to 8, with the fraction of time spent near those pole at the end of the line. `NaN` values correspond to unvisited combinations. Finally, transition in the last block are logged, with `TRANS_P` or `TRANS_T` indicating whether these were protonation or tautomer coordinate transition.


### Lambda updater debug mode

Additionally, there is also a lambda updater debug mode, which has a significant simulation speed cost:

```scheme {linenos=false,style="monokailight"}
lambda-updater-debug-mode = yes
```

This performs some extra checks and prints debug informations to the standard output.

## Reference Hamiltonian interpolation mode

While the code runs faster with the FMM-enabled fast dHd$\lambda$ calculation, it is possible to run simulation using the old implementation that runs a separate force evaluation for every form of every site. This is, of course, horrendously slow past a few sites, and should only be used for comparison purpose on small systems.

This can be used with FMM or PME electrostatics. If using PME, the coulomb modifier must be set to `none`

Because dHd$\lambda$ is computed by litteraly subtracting energies from the whole system in the different state, numerical issues become significant with larger systems, and calibration data for the FMM implementation may fail to produce exactly the same $pK_a$s in this mode. This is somewhat alleviated in double precision.


```scheme {linenos=false,style="monokailight"}
lambda-updater-evaluation-mode-selection = override-multiple-do-force
coulomb-modifier         = none
```


## Double well potential shape

The two well potential has a shape made of Hermite splines. While the typical user should likely use the default shape of the bias, some tweaking parameters are available for method developement purpose. We refer you to file `src/gromacs/constantph/implementations/SplineBias.cpp` for actual implementation.

<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/biasPotential.svg" "general_figure_image" "Two well potential with pseudo-Boltzman population histogram below" "80%;margin: auto" "block" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Two well potential </span>
    Two well potential (with a well depth difference corresponding to $RT \ln(10) (pH - pK_a$)
    </figcaption>
</figure>

As of the writing of this webpage, the following are the default for the bias shape:

```scheme {linenos=false,style="monokailight"}
constantph-bias-spline-zero-well-position = 0
constantph-bias-spline-one-well-position = 1
constantph-bias-spline-well-width = 0.2
constantph-bias-spline-inflection-point-position-coefficient = 0.5
constantph-bias-spline-derivative-at-cg-coefficient = 1.66667
constantph-bias-spline-derivative-at-df-coefficient = 0.333333
constantph-bias-spline-outer-wall-r = 20
```

The steepness of the exterior wall is controlled by `constantph-bias-spline-outer-wall-r`.
The width of the wells is controlled by `constantph-bias-spline-well-width`. 
The `coefficient` parameters are related to the relative position of the inflection points in the shape.

To explore the effect of those parameters, the interested user is advised to use the [constant pH debug mode](#debugging-informations) (see above), and observe the influence of modifications using the `debug_biasshape.X.csv` file.


## Double well potential debug mode

To explore the effect of changing double well shape parameters, as well as the barrier and well optimization algorithm, it is sometime beneficial to perform runs without considering dHd$\lambda$, where the only influence on $\lambda$ is the double well potential and the thermostat. This can be done with the following parameters:

```scheme {linenos=false,style="monokailight"}
constantph-mode          = debug_biasonlytraj
lambda-updater-evaluation-mode-selection = override-none
```

Since the MD particles have no influence on the $\lambda$ subsystem in that case, it is possible to run a minimal topology to get large amount of trajectory in a short time span. We provide [such a miniature system](/files/bias_system_minisystem.tar.gz) that can produce hundreds of nanosecond of bias-only simulation. See its HOWTO file for instructions.


