---
date: 2024-02-21T00:00:01-00:00
description: "Script to automate CPH titration simulation"
featured_image: ""
tags: []
title: "Script to automate CPH titration simulation"
weight: 100
---

<div class="maintitle">Titration script</div>

<div class="box_note">
<div class="box_note_heading">Website under construction</div>
<div class="box_note_text">
Please note that this tutorial page is currently a work in progress.
</div>
</div>

This page describes the `titrate_v3.py` Python script, which automates the setup and execution of constant-pH titration simulations. The script handles file preparation, system setup, and simulation management across multiple pH points and replicas. This documentation covers the script's usage, configuration options, and technical details.

This script is used in the [CPH Titration]({{< relref "/docs/tutorials/cph-titration" >}}) tutorial, for a practical introduction to its working.

# Download

[titration_script.tar.gz tarball](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/titration_script.tar.gz) (v3) (2025-01-23)

The archive contains:

- `titrate_v3.py`: Main script for automating titration simulations
- `titrate_cluster_launch.py`: Auxiliary script for cluster job submission, containing customizable functions for different compute infrastructures
- `lysozyme_2.yaml`: Example configuration file for a titration simulation
- `template_c36m/`: Example template directory containing:
  - Initial structure (`lysozyme.pdb`)
  - MDP template files for simulation setup
  

# How to run

This script is used in the [CPH Titration]({{< relref "/docs/tutorials/cph-titration" >}}) tutorial, for a practical introduction to its working.


The script uses a command-line interface with subcommands and a YAML configuration file:

```bash
python titrate_v3.py check_runfile -r lyzozyme_1.yaml
```

The available subcommands are :

## `check_runfile`

Verifies the YAML runfile syntax and checks for any missing required parameters.

Example:

```bash 
python titrate_v3.py check_runfile -r lyzozyme_1.yaml
```

## `prepfiles`

Creates the titration directory structure and copies template files to each pH point subdirectory. The template directory must contain all required input files, and the main titration directory (specified by `runDir`) must already exist.

Example:

```bash 
python titrate_v3.py prepfiles -r lyzozyme_1.yaml
```

See also: [`update_template` subcommand](#update_template).

## `prereplica`

Prepare the system topology, process the initial structure and solvate it. 

Example:

```bash 
python titrate_v3.py prereplica -r lyzozyme_1.yaml
```

## `replica`

Adds ions using `genion` and runs energy minimization locally (not on a compute cluster).

Note: This step may fail if buffer site placement constraints cannot be fulfilled, typically due to insufficient box size. Failure occurs during the `genion` step, but since topology files (`.top`) are modified before error detection (a limitation of `gmx genion`), the directory is left in an inconsistent state. If this happens, delete the entire titration directory and restart with a larger box.

To avoid this issue, first test your system using a single simulation as described in the [lysozyme tutorial]({{< relref "/docs/tutorials/cph-lysozyme-tuto" >}}) to determine the box size requirement for buffer sites in your system.

Example:

```bash 
python titrate_v3.py replica -r lyzozyme_1.yaml
```

See also:  [`replica_cluster` subcommand](#replica_cluster) just below.

## `replica_cluster`

Same as `replica` but submits energy minimization jobs to a compute cluster. Uses the launch function defined in `titrate_cluster_launch.py`.

```bash 
python titrate_v3.py replica_cluster -r lyzozyme_1.yaml
```

See also:  [`replica` subcommand](#replica) just above.

## `check_replica`

Verifies completion of the replica/energy minimization phase.

```bash 
python titrate_v3.py check_replica -r lysozyme_1.yaml
```


## `restart_replica`

Restarts failed energy minimization runs for specific pH points and replicas. Examines output files to identify incomplete jobs and relaunches them locally.

Example:
```bash
python titrate_v3.py restart_replica -r lysozyme_1.yaml
```

See also: [`check_replica` subcommand](#check_replica), [`restart_replica_cluster` subcommand](#restart_replica_cluster)

## `restart_replica_cluster`

Same as `restart_replica` but submits jobs to a compute cluster instead of running locally.

```bash
python titrate_v3.py restart_replica_cluster -r lysozyme_1.yaml
```

See also: [`check_replica` subcommand](#check_replica), [`restart_replica` subcommand](#restart_replica)

## `lambdaequil`

Runs lambda equilibration simulations where protein atoms are position-restrained while lambda variables equilibrate. Submits jobs to the compute cluster.

```bash
python titrate_v3.py lambdaequil -r lysozyme_1.yaml
```


## `check_lambdaequil`

Verifies completion of lambda equilibration by checking for output files.

```bash
python titrate_v3.py check_lambdaequil -r lysozyme_1.yaml
```

## `restart_lambdaequil`

Restarts failed lambda equilibration runs identified by `check_lambdaequil`.

Example:
```bash
python titrate_v3.py restart_lambdaequil -r lysozyme_1.yaml
```

## `structureequil`

Runs structure equilibration phase where both atomic positions and lambda variables are free to evolve. Submits jobs to compute cluster.

Example:
```bash
python titrate_v3.py structureequil -r lysozyme_1.yaml
```


## `check_structureequil`

Verifies completion of structure equilibration phase.

Example:
```bash
python titrate_v3.py check_structureequil -r lysozyme_1.yaml
```

See also: [`structureequil` subcommand](#structureequil)


## `continue_structureequil`

Continues interrupted structure equilibration runs from their last checkpoint files. Falls back to complete restart if no checkpoint exists.

Example:
```bash
python titrate_v3.py continue_structureequil -r lysozyme_1.yaml
```


## `restart_structureequil`

Forces complete restart of failed structure equilibration runs, ignoring any existing checkpoints.

Example:
```bash
python titrate_v3.py restart_structureequil -r lysozyme_1.yaml
```

## `prod`

Launches production runs. Jobs are submitted to compute cluster.

Example:
```bash
python titrate_v3.py prod -r lysozyme_1.yaml
```

## `check_prod`

Verifies completion of production runs.

Example
```bash
python titrate_v3.py check_prod -r lysozyme_1.yaml
```


## `continue_prod`

Continues interrupted production runs from checkpoint files. Falls back to restart if no checkpoint exists.


Example:
```bash
python titrate_v3.py continue_prod -r lysozyme_1.yaml
```

## `restart_prod`

Forces complete restart of production runs, ignoring any existing checkpoints.

Example:
```bash
python titrate_v3.py restart_prod -r lysozyme_1.yaml
```
See also: [`continue_prod` subcommand](#continue_prod) for resuming from checkpoints.

## `update_template`


Updates input files in all pH point directories by copying fresh versions from the template directory. Useful for changing simulation parameters after directory structure is created. Previously completed simulations are not affected.


Example:

```bash 
python titrate_v3.py update_template -r lyzozyme_1.yaml
```


# Writing the cluster launch functions

The `titrate_cluster_launch.py` script contains functions for submitting jobs to compute clusters. Users must modify these functions to match their local infrastructure.

## SLURM Example Implementation

The script includes a basic SLURM-based implementation using SSH for job submission (in `titrate_cluster_launch.py`):

```python
def launchOnCluster(clusterOpts, repDirectory, tprname, phase, jobname, 
                    runname, lambdaoutname, nonLambda=False, continueJob=False):
    # Replace template tokens in SLURM script
    slurm_jobfile = slurm_template.replace('$$$JOBNAME$$$', 
                                         clusterOpts["jobPrefix"] + "_" + jobname)
    slurm_jobfile = slurm_jobfile.replace('$$$GMXSRC_CLUSTER_BUILD$$$', 
                                         clusterOpts["clusterGromacsGMXRC"])
    # ... additional replacements ...

    # Write job script and submit via SSH
    jobfilename = "jobfile_{:05d}.sh".format(random.randrange(50000))
    with open(jobfilename, "w") as f:
        f.write(slurm_jobfile)
    
    command = "ssh {0}@{1} bash -l -c '\"cd {2}; sbatch {3} \"'".format(
        username, clusterOpts["clusterLoginNode"], repDirectory, jobfilename)
    os.system(command)
```

The folllowing are points to consider when writing this function:

- Job scheduler (SLURM, SGE, LSF, etc.)
- Resource specifications, which queue to submit to
- Environment setup
- Job submission commands


# Runfile parameters

The runfile uses YAML format to specify all parameters needed for the titration simulation workflow. Each section controls different aspects of the simulation setup and execution. Below is a comprehensive example followed by detailed explanations of each parameter.

## Example

A complete runfile with all required parameters is shown below. 


```yaml  {linenos=false,style="monokailight",hl_lines=[3,9,18,34,38,41]}
---
 runfileVersion: 3
 runcontrol:
    replicaNumber: 7
    lambdaEquilRunDuration: 0.1
    structureEquilRunDuration: 1
    productionRunDuration: 2
    runDir: "/lys2/"
 inputFiles:
    templateDirectory: "template_c36m"
    inputPdb: "lyzozyme.pdb"
    customLdpFile: ""
    genionMDPTemplate: "genion.mdp.template"
    emMDPTemplate: "em.mdp.template"
    lambdaEquilMDPTemplate: "lambdaequil_coupled.mdp.template"
    structureEquilMDPTemplate: "structureequil_coupled.mdp.template"
    productionMDPTemplate: "titration_coupled.mdp.template"
 systemParameters:
    boxVector: 8.0
    fmmDepth: 2
    velgenSeedBaseValue: 84521
    waterModel: "tip3p"
    prepfilesAdditionalCommands:
        - "ln -s /home/ebriand/Projects/__CPH_FF/charmm36m-lambdadyn.ff/"
    ffName: "charmm36m-lambdadyn"
    pdb2gmxAdditionalArguments: " -ignh -ldglu -ldhis -ldasp "
    sodiumName: "SOD"
    chlorideName: "CLA"
    keepawayGroup: "C-alpha"
    distResidueToBuffer: 3.0
    distProteinToBuffer: 2.0
    distBufferTolerance: 0.3
    ionConcentration: 0.150
 titration:
    phValues: [-1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0]
    mainRunBarrier: 6.0
    nstlambdaout: 250
 localGromacsLaunch:
    gromppThreads: 4
    localGromacsBinary: "/home/ebriand/Projects/gromacs_publication_builds/stable/stable_v1/bin/gmx"
 clusterJobLaunch:
    jobPrefix: "LYZ_2"
    clusterLoginNode: "moa1"
    daysForProdRun: 4
    gsubmitCmdLineAddon: " "
    clusterGromacsGMXRC: "/home/ebriand/Projects/gromacs_publication_builds/stable/cluster/stable_v1/bin/GMXRC"
    gsubmitBinary: "/home/ebriand/Projects/git-gromacs-gsubmit-build/bin/g_submit"

```



## `runfileVersion`

This is a constant checked by `titrate_v3.py`, and the [analysis tool]({{< relref "/docs/cph-postprocessing/analysis_tool" >}}) to ensure that version are matched. The current supported version is 3.

## `runcontrol` section

This section controls the overall simulation workflow, including:

### `replicaNumber`

Type: integer.

Number of replicas for each pH point. For N pH points and M replicas, the total number of simulations is \\( N \cdot M \\), with a total simulation duration of \\( N \cdot M \cdot D \\) ns (where D is the duration per replica).

Example:

```yaml  {linenos=false,style="monokailight"}
 runcontrol:
    replicaNumber: 7
```

For 7 replicas.
    
### `lambdaEquilRunDuration`

Type: float.

Duration in nanoseconds for the lambda equilibration phase, during which position restraints are applied while protonation states equilibrate. Typical values 0.1 ns.


### `structureEquilRunDuration`

Type: float.

Duration in nanoseconds for the structure equilibration phase, during which both position and protonation degree of freedom are unrestrained. Typical duration 1 to 5 ns.


### `productionRunDuration`

Type: float.

Duration in nanoseconds for the production phase, i.e. the phase where data is produced for analysis. Typical duration dependant on system. Could be 20 tp 60 ns for simple peptides, 75 to 200 ns for small proteins, up to arbitrarily long simulations for difficult to converge systems.


### `runDir`

Type: string.

Path to the directory where simulation files will be stored. The script will create subdirectories for each pH point within this location.

## `inputFiles` section

This section specifies all input files required for the simulation:

### `templateDirectory`

Type: string.

Directory containing template files and input structures.


### `inputPdb`

Type: string.

Initial protein structure in PDB format.


### `customLdpFile`

Type: string.

Optional custom `*.ldp` file. If empty, default parameters are used. The `ldp` file is used by force field developers (or those that modified the force field for use with this constant pH code) to specify which residues are protonatable, what force field files should be included in the topology for constant pH, and their reference pKa. A typical user running protein simulation will not use this type of file, except perhaps to set a different default pKa.


### `genionMDPTemplate`


Type: string.

Template MDP file for ion placement step.


### `emMDPTemplate`

Type: string.

Template MDP file for energy minimization.



### `lambdaEquilMDPTemplate`

Type: string.

Template MDP file for lambda equilibration phase.



### `structureEquilMDPTemplate`

Type: string.

Template MDP file for structure equilibration phase.

### `productionMDPTemplate`

Type: string.

Template MDP file for production phase.

### `indexFile`

Type: string.

Optional `ndx` file specifying atom groups. Used for group definitions in `mdp` files. This file will be passed to `grompp` (i.e. `-n INDEXFILE.ndx`) during the lambda equilibration, structure equilibration, and production phase of the simulation. The index file is not passed during the energy minimization phase.

See also [`keepawayGroup`](#keepawaygroup), the main purpose of this parameter.

## `systemParameters` section


Core simulation parameters defining the physical setup:


### `boxVector`

Type: float.

Box size in nanometers for the cubic simulation box.


### `fmmDepth`

Type: integer.

Depth parameter for the Fast Multipole Method algorithm used in electrostatics calculations.

See also the page [describing the FMM `mdp` parameters]({{< relref "/docs/fmm/fmm_parameters" >}})

### `velgenSeedBaseValue`

Type: integer.

Base value for random number generation in velocity initialization. All replica seeds will be this base number, plus some replica and pH-dependant value. This ensures reproducibility for initial velocities.

### `waterModel`

Type: string.

Water model to use (e.g., "tip3p", "spc"). Depends on the force field. Passed as argument to `pdb2gmx`.


### `prepfilesAdditionalCommands`

Type: list of strings.

Additional shell commands to execute during preparation phase. Typically used to add a symbolic link to the force field directory.

### `ffName`

Type: string.

Name of the force field to use.

### `pdb2gmxAdditionalArguments`

Type: string.

Additional arguments for pdb2gmx command.


### `sodiumName` and `chlorideName`

Type: string.

Residue names for sodium and chloride ions.


### `keepawayGroup`

Type: string.

Defines which atoms receive one-sided distance restraints ("keep away" restraints) relative to buffer sites. These restraints ensure buffer sites remain in solvent rather than near the protein.

Performance considerations:
- For small proteins, using `"C-alpha"` (all alpha carbons) works well
- For large proteins, the many restraints from `"C-alpha"` can impact performance
  - Solution: Create an index group with subset of C-alpha atoms (e.g., every 10th)
  - Specify this group in `indexFile` and reference it here
- For non-protein systems, choose an appropriate atom selection that represents the solute

This parameter can affects simulation speed, as the number of restraint created grows with the product of the number of buffer sites, and atom in this keepaway group. Use with care!

### `distResidueToBuffer`

Type: float.

Minimum distance in nm between titratable residues and buffer sites.


### `distProteinToBuffer`

Type: float.

Minimum distance in nm between protein and buffer sites. And by protein, it is meant the atoms in the index group specified in `keepawayGroup`.


### `distBufferTolerance`

Type: float.

Tolerance for buffer site placement distances.


### `ionConcentration`


Type: float.

Target ion concentration in mol/L.

##  `titration` section

Parameters controlling the constant-pH simulation behavior:

### `phValues`

Type: list of floats.

List of pH values at which to run simulations. Each value has its own separate simulation directory (`point_XXXX`).


### `mainRunBarrier`

Type: float.

Height of the barrier in the biasing potential for lambda dynamics during production runs, or initial barrier if using dynamic barrier optimization.


### `nstlambdaout`

Type: integer.

Frequency (in steps) for writing lambda values to output. Affects the amount of data collected for analysis and the output file size.

## `localGromacsLaunch` section

Parameters for local (non-cluster) GROMACS operations:


### `gromppThreads`


Type: integer.

Number of parallel instances for `gmx grompp` commands (and `gmx mdrun` if not running on cluster). Maximum should not exceed available CPU cores. Used exclusively for the energy minimization step.


### `localGromacsBinary`

Type: string.

Path to constant-pH enabled GROMACS binary for local operations like system preparation and `grompp`. The cluster and local GROMACS should be built from the same source, but can be different binary - this might be useful when the local workstation has a different CPU model than the cluster, such that the local version can be built with different optimization flags.

## `clusterJobLaunch` section

Custom parameters passed to cluster submission functions in `titrate_cluster_launch.py`. Contents depend on your cluster configuration and the `launchOnCluster` function implementation.

Common parameters include:
- `jobPrefix`: Prefix for job names
- `clusterLoginNode`: Hostname of cluster access node
- `daysForProdRun`: Time allocation for production runs
- `gsubmitCmdLineAddon`: Additional submission command arguments
- `clusterGromacsGMXRC`: Path to GROMACS environment on cluster

See the [cluster launch](#writing-the-cluster-launch-functions) section for implementation details.

## Optional runfile parameters

### Time step

The simulation \\( \delta t \\) is assumed to be 0.002 ps (2 fs). If it is not, set is using the `dt` parameter in the `runcontrol` section of the runfile (value in picosecond).

```yaml {hl_lines=[2],linenostart=1,style="monokailight"}
 runcontrol:
    dt: 0.002
```

# MDP template replacement tokens

The script automatically replaces tokens in MDP template files with simulation-specific values. Each token has the format `$$$$TOKEN_NAME$$$$`. Important notes:

- If a token is absent, no replacement occurs and no error is generated
- Some tokens are phase-specific (e.g., `BARRIER_HEIGHT` only for production)
- Token values come from corresponding runfile parameters
- Token placement must match GROMACS parameter names exactly

## Token `$$$$STEP_NUM$$$$`

Number of step for the simulation.

Example usage in MDP file:
```ini {linenostart=1,style="monokailight"}
nsteps                  = $$$$STEP_NUM$$$$ 
```

Used in step:
- `lambdaequil`
- `structureequil`
- `prod`

## Token `$$$$FIXED_PH$$$$`

pH for the simulation.

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
constantph-fixed-ph       = $$$$FIXED_PH$$$$
```

Used in step:
- `lambdaequil`
- `structureequil`
- `prod`


## Token `$$$$BARRIER_HEIGHT$$$$`

Barrier height of the double well potential. If using dynamic barrier optimization, starting barrier height of the simulation.

Used in step:
- `prod`

Note that, in the equilibration step, the barrier height should be set directly in the MDP file (or left to the default).

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
constantph-bias-initial-barrier = $$$$BARRIER_HEIGHT$$$$
```

## Token `$$$$STEP_NUM$$$$`

Number of simulation steps, calculated from duration specified in runfile (and timestep, if specified).

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
nsteps                  = $$$$STEP_NUM$$$$
```

## Token `$$$$LAMBDADYN_NSTOUT$$$$`

Output frequency for lambda values.

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
constantph-nstout         = $$$$LAMBDADYN_NSTOUT$$$$
```

## Token `$$$$FMM_DEPTH$$$$`

Depth parameter for Fast Multipole Method. See also the page [describing the FMM `mdp` parameters]({{< relref "/docs/fmm/fmm_parameters" >}})

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
fmm-override-tree-depth = $$$$FMM_DEPTH$$$$
```

## Token `$$$$DT_INTEGRATOR$$$$`

Integration timestep.

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
dt                      = $$$$DT_INTEGRATOR$$$$
```

## Token `$$$$KEEPAWAY_GROUP$$$$`

Group name for buffer site placement constraints.

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
constantph-buffer-site-keepaway-group = $$$$KEEPAWAY_GROUP$$$$
```

## Token `$$$$KEEPAWAY_RESIDUE_BUFFER$$$$`

Minimum distance between titratable residues and buffer sites.

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
constantph-buffer-site-keepaway-to-residue-distance = $$$$KEEPAWAY_RESIDUE_BUFFER$$$$
```

## Token `$$$$KEEPAWAY_PROTEIN_BUFFER$$$$`

Minimum distance between protein and buffer sites.

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
constantph-buffer-site-keepaway-to-protein-distance = $$$$KEEPAWAY_PROTEIN_BUFFER$$$$
```

## Token `$$$$KEEPAWAY_BUFFER_BUFFER$$$$`

Minimum distance between buffer sites.

Example usage in MDP file:
```ini  {linenostart=1,style="monokailight"}
constantph-buffer-site-keepaway-to-buffer-distance = $$$$KEEPAWAY_BUFFER_BUFFER$$$$
```

# Common issues and solutions


- Missing files: Check template directory contents and paths
- Failed energy minimization: Box size too small for buffer sites
- Failed checkpoints: Use `continue_*` commands rather than `restart_*`
- MDP errors: Use `update_template` after fixing template files
