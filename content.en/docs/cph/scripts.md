---
date: 2024-01-15T00:00:01-00:00
description: "Script to automate CPH simulations"
featured_image: ""
tags: []
title: "Script to automate CPH simulations"
weight: 100
---

<div class="maintitle">Script to automate CPH simulations</div>

<div class="box_note">
<div class="box_note_heading">Website under construction</div>
<div class="box_note_text">
Please note that this page is currently a work in progress.
</div>
</div>


Scripts and snippets to automate constant pH simulation and analysis

# Titration script: `titrate_v3.py`

A script to run titrations is available on its [own page]({{< relref "/docs/cph/titration_script" >}}). This script is used in the [CPH Titration]({{< relref "/docs/tutorials/cph-titration" >}}) tutorial, for a practical introduction.

# Calibration

This set of script automates the running of the calibration simulation (but not the modification of the forcefield files to add new residues; nor the final polynomial fit)

- The `ti_pointwise.py` script
- A template directory: here are charmm36m templates and amber99sb*-IDLN templates
- A runfile example to modify

**TODO**: Upload scripts and add download link above

See the [Calibration Script HOWTO]({{< relref "/docs/cph/calibration_script_howto" >}}) for instruction on how to use this script.

For help regarding the (manual) previous step of adding a new protonatable residues to the forcefield files, see [Adding a custom protonatable residue]({{< relref "/docs/cph/custom-res" >}}).

For the final polynomial fit, see [Polynomial fit to calibration data]({{< relref "/docs/cph/polynomial_fit_calibration_data" >}}).

# Misc snippets


## FMA
