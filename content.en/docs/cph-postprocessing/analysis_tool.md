---
date: 2024-01-15T00:00:01-00:00
description: "Analysis tool for CPH simulation"
featured_image: ""
tags: []
title: "Analysis tool for CPH simulation"
weight: 100
---

<div class="maintitle">Analysis tool for CPH simulation</div>

As a companion to the constant pH code, an analysis tool named `LambdaAnalysis` can be used to process the constant pH trajectories.

# Installing the analysis tool (binary)

The easiest way to install the tool is using the compiled binary (If needed, instruction to build from source are [available, including a Docker option](#building-analysis-tool-from-source).



Download the [LambdaAnalysis tarball](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/LambdaAnalysis_v3.tar.gz) (v3)


You can check that the tool works by running:

```bash
./LambdaAnalysis
```

Which should return:

```{linenos=false,style="solarized-light"}
Lambda dynamics output analysis
Global options:
  --command arg         command to execute
  --subargs arg         Arguments for command

```


## Troubleshooting dependency problem


### C library / `glibc`
If you encounter issue with `GLIBC` symbols, like so:

```{linenos=false,style="solarized-light"}
./LambdaAnalysis: /lib64/libc.so.6: version `GLIBC_2.38' not found (required by ./LambdaAnalysis)
```

this indicates that your C library could be too old. The executable above was built for `glibc` 2.34. You can check your version with:

```bash
ldd --version
```

We offer a version of the executable for glibc 2.31 and above:

[LambdaAnalysis tarball](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/LambdaAnalysis_glibc_3_31.tar.gz) (v3)

If your `glibc` version is even older, and not compatible with this, see [the instruction to build the tool from source, or to use Docker.](#building-analysis-tool-from-source).

### Missing library `libtbb`
If you encounter error messages related to missing libraries, such as

```{linenos=false,style="solarized-light"}
LambdaAnalysis: error while loading shared libraries: libtbb.so.12: cannot open shared object file: No such file or directory
```

this indicates that some of the dependencies for this tool are missing. The provided binary depends on Threading Building Blocks (Ubuntu package `libtbb-dev`), and virtually-universal `libz` and `libpthread`. 

```{linenos=false,style="solarized-light"}
LambdaAnalysis 
├── libz.so.1 [LD_LIBRARY_PATH]
├── libtbb.so.12 [runpath]
│   └── libpthread.so.0 [default path]
└── libpthread.so.0 [default path]
```

Threading Building Blocks can often be installed through your package manager. 
Otherwise, we provide a pre-built library in the compressed archive that can be added to the library search path through `LD_LIBRARY_PATH` (e.g. ``export LD_LIBRARY_PATH=`pwd`:$LD_LIBRARY_PATH`` if in the directory where `libtbb.so.12` has been extracted from the archive). Otherwise , ``LD_LIBRARY_PATH=/path/to/directory/where/archive/was/extracted/:$LD_LIBRARY_PATH``


# Using the analysis tool


This post-processing tool has the following general command syntax:

```bash
./LambdaAnalysis subcommand  --flag1 argument1  --flag2 argument2
```


<div class="box_note">
<div class="box_note_heading">Binary cache files (<code>.bindata</code>)</div>
<div class="box_note_text">
<p>In many cases, the <code>LambdaAnalysis</code> tool creates a binary version of the CPH trajectories (<code>.dat</code> files) it reads, identically named except for a <code>.bindata</code> suffix. Subsequent reading of the same file then uses the cache rather than the original trajectory, with significantly improved performance.</p>

<p>
However, if the content of the <code>.dat</code> file has changed, for instance if an interim analysis was conducted while the simulation was still running, or the simulation was prolonged, it is necessary to remove the corresponding <code>.bindata</code> file so that the new data is used. Generally, removing the cache file with a command like <code>rm *.bindata</code> is safe (at worst, it will slow down the next execution while the cache files are regenerated).
</p>
</div>
</div>

<div class="box_note">
<div class="box_note_heading">Undocumented commands and argument</div>
<div class="box_note_text">
<p>
The subcommands and argument which are documented here have been validated. There are additional subcommands and argument in this tool 
for development use, which might not be at the same level of quality. We recommend not to use those.
</p>
</div>
</div>

## Numpy array for a single file

This transforms a single CPH output file (`.dat`) into a numpy array file (`.npy`). 

The `LambdaAnalysis` tool reads `.dat` files significantly faster than an equivalent python program, such that using this subcommand is recommended even if further processing will be done using one's own python script rather than this tool.

```bash
./LambdaAnalysis export_npy \
        --input ./single_asp_1/point_0002/titr_s0000.dat \
        --output example_traj_data.npy
```

| Flag               | Function                                                  |
|--------------------|-----------------------------------------------------------|
| `--input`          | Path to input `.dat` file                            |
| `--output`         | Path to the output file (`.npy` array file)            |
| `--force`          | Overwrite files and proceed even if large files would be generated, or much RAM used. |


### Output format

The resulting array can be loaded in python with:

```python {linenos=false,style="monokailight"}
import numpy as np
data = np.load("example_traj_data.npy")
```

The axis of the array are (time, site index, data column index). The site index follows the order of declaration in the topology file `[ constant_ph ]` section.


| Data column index  | Data                                          |
|--------------------|-----------------------------------------------|
| 0                  | Time   (ps)                                   |
| 1                  | $\lambda_p$                                   |
| 2                  | $\lambda_t$                                   |
| 3                  | $\partial{\mathcal{H}}/\partial{\lambda_p}$   |
| 4                  | Force field compensation  $V_\mathrm{mm}$     |




## Titration analysis

Computes information necessary to determine pKa and plot titration curves based on a titration simulation run with the [Titration scripts]({{< relref "/docs/cph/scripts" >}}), with the corresponding runfile path provided as `--runfile` argument.


```bash
./LambdaAnalysis titration \
        --runfile /home/user/titrations/a99_snase_fmm_2.yaml \
        --do_transition_count 1 \
        --output a99_snase_fmm_2.csv
```

| Flag               | Function                                                  |
|--------------------|-----------------------------------------------------------|
| `--runfile`                | Path to the runfile from the titration script (`.yaml` file) |
| `--do_transition_count`    | Recommended: counts the transitions during the trajectory.              |
| `--output`                 | Path to the CSV file to be written, with the titration data |
| `--maxtime`                 | Consider data only up to the time (in ps) specified as argument.  |


Note that `--do_transition_count` slightly slows down the process, but is necessary to get accurate 
transition count. 
If not planning on using the transition counts in downstream analysis, a few minutes can be saved
by running without. In that case, an approximate transition count (instead of exact) will be found in
the output file.

The option `--maxtime` can be used to compare simulation with different runtime, by discarding all data past a certain duration (in nanonsecond, starting at zero).


### Custom directory structure

If the titration was run without using the titration script, i.e. manually or using your own titration script, 
there is no runfile to be used as input.
The `titration` subcommand can still be used however, by providing a CSV (comma-separated values) file with the pH values and the path to the output `.dat` file, like so:

``` {linenos=false,style="monokailight"}
2.0,/home/ebriand/Projects/titr_example_data/ph_2_0.dat
2.5,/home/ebriand/Projects/titr_example_data/ph_2_5.dat
3.0,/home/ebriand/Projects/titr_example_data/ph_3_0.dat
3.5,/home/ebriand/Projects/titr_example_data/ph_3_5.dat
3.75,/home/ebriand/Projects/titr_example_data/ph_3_75.dat
4.0,/home/ebriand/Projects/titr_example_data/ph_4_0.dat
4.5,/home/ebriand/Projects/titr_example_data/ph_4_5.dat
5.0,/home/ebriand/Projects/titr_example_data/ph_5_0.dat
5.5,/home/ebriand/Projects/titr_example_data/ph_5_5.dat
6.0,/home/ebriand/Projects/titr_example_data/ph_6_0.dat
6.5,/home/ebriand/Projects/titr_example_data/ph_6_5.dat
```

The titration command can the be run while specifying the path to this file:

```bash
./LambdaAnalysis titration \
        --resultspathfile ./results_path.txt \
        --do_transition_count 1 \
        --output titr_example.csv
```

### Output format

The output file is a CSV file with header as first line.
See [the Jupyter Notebook section](#jupyter-notebook) for example of downstream analysis using the CSV file.


## Mutual information analysis

Computes normalized [Mutual Information](https://en.wikipedia.org/wiki/Mutual_information) between the trajectories for all sites, for each replica and pH values.

```bash
./LambdaAnalysis mutual_information_titration \
        --runfile /home/user/titrations/a99_snase_fmm_2.yaml \
        --output a99_snase_fmm_2_MI.csv
```


| Flag               | Function                                                  |
|--------------------|-----------------------------------------------------------|
| `--runfile`                | Path to the runfile from the titration script (`.yaml` file) |
| `--resultspathfile`                | Alternative to the runfile - see [above](#custom-directory-structure) |
| `--output`                 | Path to the CSV file to be written, with the Mutual Information data |



The trajectory is binarized internally to the protonated/deprotonated state based on a $\lambda_p > 0.5$ threshold.

### Output format

The output file is a CSV file with header as first line, with the following columns:

| Column name           | Content                                                   |
|-----------------------|-----------------------------------------------------------|
| `site`                | Index (0-based) of the first site of the considered pair |
| `pH`                  | pH value at which the replica trajectory was run             |
| `otherSite`           | Index of the second site of the pair|
| `entropy_site`        | Information theoretic entropy for the binarized trajectory (site 1) |
| `entropy_othersite`   | Same for the other site |
| `rawMutualInformation`| Mutual information metrics (not normalized to 0-1 range) |
| `normalizedMutualInformation`| Normalized mutual information metric |
| `filename`            | Path to the `.dat` file from which the trajectory data was obtained |
| `replica`             | Index (0-based) of the replica |

As seen, each replica at each pH point are treated separately. 

The formula for the Normalized Mutual Information (with $\mathrm{MI}$ the mutual information and $\mathrm{H}$ the entropy) is

$$
    \mathrm{NMI}(X,Y) = \frac{2 \\, \mathrm{MI}(X,Y)}{\mathrm{H}(X) + \mathrm{H}(Y)}
$$

Note that normalized mutual information is not defined when both \\( \mathrm{H}(X) \\) and \\( \mathrm{H}(Y) \\) are zero, which occurs when both trajectory only contain a single state (i.e. no protonation transition).
In that case, the NMI value in the output file is set to `nan`. Such values can either be discarded, or set to 0 (no coupling), in the downstream analysis, depending on the criteria used for positive coupling.
In any case, to avoid false positive, coupling should be evaluated only when the entropy of the trajectories is higher than some threshold (in our publication 0.1) for the mean entropy over all replicas at a given pH point, which also avoids such undefined NMI issue.


## Microstate titration

Find and count the population of the protonation microstate for the whole protein, not for individual residue. For instance, a protein with 2 Glu sites has 4 microstates: both deprotonated (`00`), both protonated (`11`), only one protonated (`01` or `10`). This subcommand is notably useful to analysis coupling between residues quantitatively.

As a mode of the `titration` subcommand, it also supports the `--resultspathfile` argument documented above.

```bash
./LambdaAnalysis titration \
        --runfile /home/user/titrations/a99_snase_fmm_2.yaml \
        --microstate 1 \
        --output microstate_a99_snase_fmm_2_MI.csv
```

### Output format

The output file is a CSV file with header as first line, with format similar to:

```{linenos=false,style="solarized-light"}
pH,replica,microstate,count,total_count
1,0,"0011011111110110111",39,150001
1,0,"0011011111110110211",17,150001

```

The columns are as follow:
| Column name           | Content                                                   |
|-----------------------|-----------------------------------------------------------|
| `pH`                  | pH value at which the replica trajectory was run             |
| `replica`             | Index (0-based) of the replica |
| `microstate`           | String describing the microstate |
| `count`        | Number of frame counted in the given microstate in that simulation run |
| `total_count`   | Total number of frame for the given (ph,replica) simulation run |

The string describing the microstate always has the same number of character, namely, the number of titratable site in the analyzed trajectory. This means that the microstate string sometime has leading zeros - care should be taken when parsing the CSV file to not interpret this string as a number, leading to dropping those leading zeros.

The binary value for a microstate is assigned to `0` or `1` based on a $\lambda_p > 0.5$ threshold, while the state `2` is assigned when the site's trajectory is censored due to recent adjustment of the barrier or well position. Generally, the microstates containing `2` state can be disregarded as of negligible population, when considering the protonation state of a small number of site (up to 3-4). However, discarding all microstate containing a `2` will lead to considerable sampling loss for large systems. This is because, while the probability for any given site to be in the `2` state is low, the probability of any of the site to be in `2` is not so low. This censoring indicating should therefore be considered site-wise, or small-group-wise.


# Jupyter notebook

Many of the outputs of `LambdaAnalysis` must be further processed into figures or table for human interpretation. [A jupyter notebook](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/Titration_lyzozyme_example.ipynb) is provided to read the CSV files.

The input files path as well as the characteristics of the titration site (residue type, optionally experimental pKa) are to be specified in the `Input file` section of the notebook. 

<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/jupyter_notebook_titration_param.png" "general_figure_image" "Screenshot of the parameter section of the jupyter notebook" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Parameter section of the Jupyter notebook</span>
    </figcaption>
</figure>


The rest of the analysis is commented, and ultimately produces pKas and titration curves:


<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/lysozyme_1_titration_site_000.png" "general_figure_image" "Titration curves for the lysozyme example notebook" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Titration curve produced by the notebook</span>
    </figcaption>
</figure>
<br />
<br />
<figure class="general_figure" style="width: 80%">
    {{< baseimg1 "/images/lysozyme_1_CPH_vs_Ref_allres_ConfInt.png" "general_figure_image" "Titration curves for the lysozyme example notebook" "" "" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >pKa plot produced by the notebook</span>
    </figcaption>
</figure>

# Previous versions of Analysis tool

Downloadable for reproducibility purpose - please use the above newest release for new projects.

v0.1 (beta) version: [LambdaAnalysis tarball](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/LambdaAnalysis.tar.gz)  and [source for it](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/LambdaAnalysis_source.tar.gz)




# Building analysis tool from source

## Using docker

We provide Dockerfile to build the LambdaAnalysis and all its dependencies in a reproducible environement:

- [Dockerfile for v3](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/docker/LambdaAnalysis_v3/Dockerfile) (v3) 

And the corresponding docker image [on the Docker hub](https://hub.docker.com/r/elianebriand/lambda-analysis/tags):

- Latest: `docker pull elianebriand/lambda-analysis`
- v3: `docker pull elianebriand/lambda-analysis:v3`

In both of these cases, the `LambdaAnalysis` binary, and its dependency `libtbb` is in the directory `/universal_build/lambdaanalysis/build`. You can then mount your data directory and drop in the container. Here, assuming your titration data is in `/home/user/cph_data/`, we mount it to `/universal_build/data` in the container, and start a bash shell.

```bash
docker run -it --entrypoint /bin/bash -v /home/user/cph_data/:/universal_build/data  elianebriand/lambda-analysis:v3
```

We can then call, from inside the container shell, the analysis tool:

```bash
./LambdaAnalysis export_npy \
        --input /universal_build/data/single_asp_1/point_0002/titr_s0000.dat \
        --output /universal_build/data/example_traj_data.npy
```

And get back our output `example_traj_data.npy` in `/home/user/cph_data/` outside the container.

Note that the `LambdaAnalysis` binary generated is statically linked, so if your C library (`glibc`) is recent enough, you can simply copy it (and dependency `libtbb.so.12`) from container to host with:

```bash
docker cp <containerId>:/universal_build/lambdaanalysis/build/LambdaAnalysis ./
docker cp <containerId>:/universal_build/lambdaanalysis/build/libtbb.so.12 ./
```

## Direct build

The following dependencies must be installed (in development version) to build the analysis tool from source. The corresponding package name are specified for ubuntu, when known.

- oneAPI Threading Building Blocks (Ubuntu `libtbb-dev`)
- BLAS and LAPACK (Ubuntu `libblas-dev liblapack-dev`)
- Armadillo linear algebra library (Ubuntu `libarmadillo-dev`)
- yaml-cpp (Ubuntu `libyaml-cpp-dev`)
- C++ `fmt` library (Ubuntu `libfmt-dev`)
- C++ `spdlog` library (Ubuntu `libspdlog-dev`)
- C++ Boost library (version 1.50 or greater - Ubuntu `libboost-all-dev`)
- lmfit ([source](https://jugit.fz-juelich.de/mlz/lmfit) - No ubuntu package to our knowledge)
- Vc SIMD library (Ubuntu `vc-dev`)
- zlib (Ubuntu `zlib1g-dev`)
- As well as `libgfortran`, `libm`, `libquadmath` and `libpthread` which should already have been installed with your compiler.

See also the Dockerfiles above, if desiring to install those package from source (which allows the production of static binary.)

The `lmfit` package must be built from source, then installed somewhere (use `-DCMAKE_INSTALL_PREFIX=/where/to/install` to select installation directory).

Download the [LambdaAnalysis source tarball](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/LambdaAnalysis_source_v3.tar.gz). (v0.1)

Build is typical:

```
mkdir build && cd build
cmake ..
make
```

You may add the following argument to cmake, notably if libraries fail to be found:
- `-Dlmfit_ROOT=/where/lmfit/was/installed` to point to the `lmfit` installation directory. This installation directory should have subdirectory `include` and `lib` or `lib64`. Alternatively, `-Dlmfit_INCLUDE_DIR=/usr/include/` and `-Dlmfit_LIBRARY_DIR=/usr/include/`
- `-DCOMPILER_LIBS_HINT=/directory/with/various/lib` to point to where `libgfortran`, `libquadmath` and `libpthread` are located. This directory should directly contain the binary library file, such as `libgfortran.a` or `libgfortran.so`.
- `-DTARGET_ARCHITECTURE="broadwell"` which sets the target microarchitecture for the build. This is only useful if building on a different machine from where the tool will be run. You might also consider adding something like `-DCMAKE_CXX_FLAGS="-march=broadwell -fPIC -static-libgcc -static-libstdc++ " -DCMAKE_C_FLAGS="-march=broadwell -fPIC -static-libgcc "` for a build likely to run on all modern machines.
- `-DBoost_INCLUDE_DIR=/usr/include/` and `-DBoost_LIBRARY_DIR=/usr/lib/` if using a non-standard directory for the Boost library.

