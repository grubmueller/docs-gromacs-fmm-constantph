---
title: "Protonation FMA"
date: 2017-03-02T12:00:00-05:00
---

<div class="maintitle">Protonation FMA</div>

<div class="box_note">
<div class="box_note_heading">Website under construction</div>
<div class="box_note_text">
Please note that this page is currently a work in progress.
</div>
</div>


While a benefit of constant pH simulation is a more accurate MD simulation through dynamic protonation, another is the possibility to explore what structural factors might be related to the protonation change, either simply to better understand the protonation dynamics, or to correlate it with relevant catalytic or biological effects.

The FMA analysis is based on a titration of the protein of interest. The following workflow consider a single residue within that protein, and should be repeated for each residues of interest within the protein.

# General

<figure class="general_figure">
    {{< baseimg1 "/images/explain_fma.png" "general_figure_image" "Flowchart describing the FMA workflow" "" "" >}}
    <figcaption class="general_figure_caption"><span class="general_figure_caption_head" >Flowchart describing the FMA worklow</span> as covered in this page</figcaption>
</figure>


**TODO**: Describe the protonation FMA workflow

# Citation

This FMA workflow is published in:

```bibtex
Bibtext here
```

# Obtaining the FMA tool

## Binary

We mirror two binary versions of the FMA tool, [a first one](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/fma_bin/gmx)  and [a second one](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/files/fma_bin/fma.tgz). 

A few notes:
- The first one is used with a command like `gmx fma <rest of arguments>` while the second is used with `g_fma <rest of arguments>`.
- You might need to install `BLAS` and `libgomp`, if getting missing library errors.
- If one fails to run on your machine, please try the other one, then finally you might try to build the tool from source (see below)


## Compiling  from source

The PLS FMA tool can be downloaded [on the page of the de Groot group](http://www3.mpibpc.mpg.de/groups/de_groot/fma.html), (or [alternatively here](/files/g_fma-beta.tar.gz), should the link become dead).

It has been reported that certain BLAS libraries are incompatible with this source, though they might successfully build. The tool will then return completly non-sensical results like so:

```
minimal projection = 340282346638528859811704183484516925440.000
maximal projection =    0.000
r(training)   =     -nan
r(validation) =     -nan
``` 

In this case, try the binary versions above, or try another BLAS library.

**TODO**: Details on how to compile - IIRC we need a old version of gromacs for this tool to work.


# Preparing the files

We assume that we are analyzing files obtained by running [our titration script]({{< relref "/docs/cph/titration_script" >}}), in term of filenames and folder structure, though we will explain all commands such that you can translate them to your own structure, if you are using a different automation mechanism.

We define a few variables to shorten the subsequent commands:

```bash
# Where the point_0000 directory are stored - the base directory of the titration
BASEDIR_TITRATION="~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/"
# The YAML file used to run the titration
RUNFILE="~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2.yaml"
```

We need to set the index of the protonatable residue of interest:

```bash
# Zero-based index of the protonatable residue - here, the 4th protonatable residue (not index in protein sequence!)
PROTRES_INDEX=3
```

We also need to consider the titration curve for the residue:

{{< baseimg1 "/images/cardiotoxin_glu17.png" "general_figure_image" "Example of a titration curve obtained by constant pH titration" "70%;margin: auto" "block" >}}



The FMA needs a collection of trajectories where the protonated and deprotonated states are represented somewhat equally, so that conformation characteristic of both of these state are found. Therefore, we recommend selecting the pH point closest to the p$K_a$ of the residue, for instance here pH $= 4.0$. Though this is not covered here, it is also possible to include pH point farther from the p$K_a$. For this page, the index of the point is 5, as set here:


```bash
# The index of the choosen pH point
PHPOINT=5
```

In the course of the analysis, we will look over the pH points and replica, therefore we set the bounds of these loops:

```bash
# Last index for the replicas, so num replicas - 1 (39 for 40 replicas)
REPLICA_MAX=39
# Last index for the pH point, so 13 for 14 pH points
PH_POINT_MAX=13
```


## Matching structure and $\lambda$ trajectories

For implementation reason, we need a set of file containing one $\lambda$ values for each frame of each structure trajectory we will analyse with FMM. 

Since we generally set the $\lambda$ output frequency (in the `.dat` file) much higher than the structure output frequency (in the `.xtc` file, controlled by `nstxout-compressed`), for reason of space savings, we cannot directly use our `.dat` file. It also contain a lot of data we will not use for the FMA, such as $\lambda$ velocities, or the state of buffer sites.

Our constant pH implementation contains a small utility for the purpose of matching both of these `gmx lambda_traj`. We will loop over all the replicas made at our selected pH point, and call `gmx lambda_traj` on each:

```bash
pointnum=$(printf "%04d" $PHPOINT)
for ((i=0;i<=REPLICA_MAX;++i));do
    shardnum=$(printf "%04d" $i)
    
    gmx lambda_traj  -f "$BASEDIR_TITRATION"/point_"$pointnum"/titr_s"$shardnum".xtc \
                        -o target_titr_s"$shardnum".xvg \
                        -ldtraj "$BASEDIR_TITRATION"/point_"$pointnum"/titr_s"$shardnum".dat \
                        -s "$BASEDIR_TITRATION"/point_"$pointnum"/titr_s"$shardnum".tpr \
                        -lambdasite $PROTRES_INDEX
done;
```

This produces a series of file of the form `target_titr_sXXXX.xvg` containing timestamp and $\lambda$ value for the residue of interest.


## Concatenating trajectories

We need a single trajectory file to input into the FMA. We will therefore concatenate all of the `.xtc` files using `gmx trjcat`:


```bash
pointnum=$(printf "%04d" $PHPOINT)
gmx trjcat -cat -f "$BASEDIR_TITRATION"/point_"$pointnum"/titr_s"$shardnum".xtc -o bigtraj.xtc
```

We obtain a large `bigtraj.xtc` file. `trjcat` will print the number of frame in the final trajectory.


We need to perform the same style of concatenation for the `.xvg` files containing the $\lambda$ values, as well as discard the XVG header:

```bash
cat target_titr_s00*.xvg > target_fulltitr.xvg 
cat target_fulltitr.xvg | awk '/^[^#^@]/' > target_fulltitr_nocomment.xvg
```

We can now compare the number of line in the concatenated XVG and the number of frame `bigtraj.xtc`, which should be equal:

```bash
# Count line in target_fulltitr_nocomment.xvg
wc -l target_fulltitr_nocomment.xvg
```

## `TPR` file for analysis

We need to create a `tpr` file for analysis purpose, that is different from the `tpr` used in the simulation. This is because the distance restraint used during the simulation will cause problems with `trjconv`.

To that end, we copy the structure file `.gro`, the topology `.top` and a `.cpt` file to the present directory 

```bash
cp ... # complete this
```

In the `mdp` file, we add/modify the following line:

```ini   {linenos=false,style="monokailight"}
constantph-buffer-site-keepaway-group = none
```
We then call `grompp`

```bash
gmx grompp  -f analysis_mdp.mdp -t analysis_cpt.cpt -o analysis_tpr.tpr -p analysis_topol.top -c template.gro -r template.gro
```

## PBC removal and fitting

We can now further process `bigtraj.xtc`, obtained above with `trjcat`.

```bash
echo "0\n" | gmx trjconv -f bigtraj.xtc -pbc mol -s analysis_tpr.tpr -o bigtraj_pbc.xtc

echo "4\n0\n" | gmx trjconv -f bigtraj_pbc.xtc -fit rot+trans -s analysis_tpr.tpr -o bigtraj_fit_prelim.xtc

echo "0\n" | gmx trjconv -f bigtraj_fit_prelim.xtc -dump 50000 -s analysis_tpr.tpr -o frameMid_fitted_prelim.pdb
```

We obtain `bigtraj_fit_prelim.xtc`, which has PBC removed, and is fitted on the last frame of the production trajectory, and `frameMid_fitted_prelim.pdb`, a PDB file corresponding to a frame in the trajctory. Both of these are temporary files, to be used in the next step but not for the FMA itself.

## Obtaining an average structure

We call the FMA tool a first time, just to obtain an average structure (one of the output of the )

```bash
echo "4\n2\n" | g_fma  -f bigtraj_fit_prelim.xtc -s frameMid_fitted_prelim.pdb  -y target_fulltitr_nocomment.xvg -v fma_vect_prelim.trr -o fma_mode_prelim.xvg -xvg xmgrace -normpbc -pbc -dim 20 -ntrain 30000 -ref fma_ref_prelim.pdb 
```

We then fit the trajectory to that average structure again:

```bash
echo "4\n0\n" | gmx trjconv -f template.gro -fit rot+trans -s fma_ref_prelim.pdb -o template_fitted.gro


gmx grompp -f analysis_mdp.mdp -t analysis_cpt.cpt -o analysis_tpr_fitted.tpr -p analysis_topol.top -c template_fitted.gro -r template_fitted.gro -maxwarn 1

echo "4\n0\n" | gmx trjconv -f bigtraj_pbc.xtc -fit rot+trans -s analysis_tpr_fitted.tpr -o bigtraj_fit.xtc

echo "0\n" | gmx trjconv -f bigtraj_fit.xtc -dump 50000 -s analysis_tpr_fitted.tpr -o frameMid_fitted.pdb
```

The point of obtaining an average structure, and fitting our trajectory to it, is to ensure the FMA values are normalized to values near 0-1, which would not necessarily be the case if we used any arbitrary frame for the fitting. It is not formally necessary, but it makes the rest of the workflow easier to standardize.


# Performing the FMA

Lo and behold, we are now ready for the FMA analysis itself:


```bash
echo "4\n2\n" | g_fma  -f bigtraj_fit.xtc -s fma_ref_prelim.pdb  -y target_fulltitr_nocomment.xvg -v fma_vect.trr -o fma_mode.xvg -xvg xmgrace -normpbc -pbc -dim 20 -ntrain 2250 -ref fma_ref.pdb

```

Save the output of this command to a file, in particular this section:

``` {linenos=false,style="solarized-light"}
Fitted on backbone, protein-h for PCA basis

Reading frame    3000 time 36000.000   
Analyzed 3040 frames, last time 75000.000
Running PLS on 2250 training structures (of 3040 total structures)

The average value of y (in the training set) is 0.561 .

r(training)   =    0.889
r(validation) =    0.695
```


## Quality control

As described above, we need a mix of protonated and deprotonated state in the trajectory, so that FMA can find the motion most correlated to the protonation state. We thus need the `The average value of y (in the training set) ` reported in the ouput of the tool to be around 0.5 --- say 0.3 to 0.7.

We also need to have a validation set of about 25 percent, which we can check with this line:

``` {linenos=false,style="solarized-light"}
Running PLS on 2250 training structures (of 3040 total structures)
```

The validation set proportion is here `1 - (2250/3040)`, which is `0.25`, as desired.

Finally, the FMA tool reports on the r-value for the PLS, for both the training and validation set.

``` {linenos=false,style="solarized-light"}
r(training)   =    0.889
r(validation) =    0.695
```

If the validation r-value is near 0 (e.g. less than 0.2), while the training r-value is high (e.g. more than 0.8), there is overfitting. 

**TODO**: explain what to do to solve overfitting (use less atoms, maybe only C-alpha, or nearby C-alpha for the FMA)

To obtain the explained variance, compute `R^2` for the validation set.

# FMA histogram


# High and low FMA structure


# Projecting all replicas separately


# FMA-binned titration


# Element of intepretation

## Qualitative analysis of the high and low FMA structure

## p$K_a$ as a function of FMA


# Remarks on correlation and causation for protonation

The FMA analysis presented here is purely correlational: the protonation state is correlated to the structure (and vice-versa). This is interesting, but insufficient to determine causation: after all, it is possible for the protonation state to be influence by the structure, or for the structure change to follow from changing protonation state, or even that both are involved in a mutual feedback.

To go beyond correlational analysis, we have to perturb the system: either by freezing the protonation state, which can be done either with normal MD, or by the `ti` mode of our implementation of constant pH. Or, by forcing the conformation to a known state along the FMA MCM axis. This is called [Essential Dynamics](https://manual.gromacs.org/current/reference-manual/algorithms/essential-dynamics.html).


# Raw FMA worklow

Raw HOWTO from which this page is written from:


```
 
mkdir a99_lysozyme_asp_48
cd a99_lysozyme_asp_48
ln -s /home/ebriand/Projects/__CPH_FF/amber99sb-star-ildn-lambdadyn.ff/ amber99sb-star-ildn-lambdadyn.ff

ph = 1.5 is near pKa
point 0005

gmx trjcat -cat -f ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_0005/titr_s<0000-0039>.xtc -o bigtraj.xtc


cp ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_0005/titr_s0000.gro template.gro
cp ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_0005/titr_s0000.mdp analysis_mdp.mdp
cp ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_0005/restrainedequil_s0000.cpt analysis_cpt.cpt
cp ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_0005/topol_s0000.top analysis_topol.top

# modify analysis mdp c-alpha to none

gmx grompp -f analysis_mdp.mdp -t analysis_cpt.cpt -o analysis_tpr.tpr -p analysis_topol.top -c template.gro -r template.gro

echo "0\n" | gmx trjconv -f bigtraj.xtc -pbc mol -s analysis_tpr.tpr -o bigtraj_pbc.xtc

echo "4\n0\n" | gmx trjconv -f bigtraj_pbc.xtc -fit rot+trans -s analysis_tpr.tpr -o bigtraj_fit_prelim.xtc

echo "0\n" | gmx trjconv -f bigtraj_fit_prelim.xtc -dump 50000 -s analysis_tpr.tpr -o frameMid_fitted_prelim.pdb

pointnum=$(printf "%04d" 5)
for i in {0..39}; do;
    shardnum=$(printf "%04d" i)
    
    gmx lambda_traj  -f ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/titr_s"$shardnum".xtc \
                        -o target_titr_s"$shardnum".xvg \
                        -ldtraj ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/titr_s"$shardnum".dat \
                        -s ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/titr_s"$shardnum".tpr \
                        -lambdasite 5
done;


cat target_titr_s00*.xvg > target_fulltitr.xvg 
cat target_fulltitr.xvg | awk '/^[^#^@]/' > target_fulltitr_nocomment.xvg
wc -l target_fulltitr_nocomment.xvg

# Just to get a averaged structure

echo "4\n2\n" | /home/ebriand/tmp/fma/g_fma  -f bigtraj_fit_prelim.xtc -s frameMid_fitted_prelim.pdb  -y target_fulltitr_nocomment.xvg -v fma_vect_prelim.trr -o fma_mode_prelim.xvg -xvg xmgrace -normpbc -pbc -dim 20 -ntrain 30000 -ref fma_ref_prelim.pdb 


echo "4\n0\n" | gmx trjconv -f template.gro -fit rot+trans -s fma_ref_prelim.pdb -o template_fitted.gro


gmx grompp -f analysis_mdp.mdp -t analysis_cpt.cpt -o analysis_tpr_fitted.tpr -p analysis_topol.top -c template_fitted.gro -r template_fitted.gro -maxwarn 1


echo "4\n0\n" | gmx trjconv -f bigtraj_pbc.xtc -fit rot+trans -s analysis_tpr_fitted.tpr -o bigtraj_fit.xtc

echo "0\n" | gmx trjconv -f bigtraj_fit.xtc -dump 50000 -s analysis_tpr_fitted.tpr -o frameMid_fitted.pdb

echo "4\n2\n" | /home/ebriand/tmp/fma/g_fma  -f bigtraj_fit.xtc -s fma_ref_prelim.pdb  -y target_fulltitr_nocomment.xvg -v fma_vect.trr -o fma_mode.xvg -xvg xmgrace -normpbc -pbc -dim 20 -ntrain 2250 -ref fma_ref.pdb


# Save the run data (explained variance et al)

Fitted on backbone, protein-h for PCA basis

Reading frame    3000 time 36000.000   
Analyzed 3040 frames, last time 75000.000
Running PLS on 2250 training structures (of 3040 total structures)

The average value of y (in the training set) is 0.561 .

Back Off! I just backed up fma_ref.pdb to ./#fma_ref.pdb.2#
r(training)   =    0.889
r(validation) =    0.695

echo "2\n" |gmx trjconv -f bigtraj_fit.xtc -dump 50000 -s analysis_tpr_fitted.tpr -o proteinheavy.pdb


mv fma_vect.trr fma_vect_proteinheavy.trr

echo "4\n2" | gmx anaeig -s analysis_tpr_fitted.tpr -f bigtraj_fit.xtc -v fma_vect_proteinheavy.trr -proj fma_proteinheavy_all.xvg -first 1 -last 1


xmgrace fma_proteinheavy_all.xvg

Low 5th are < -0.11310 | high 95th are > 1.18453

limit: 0.0 0.75

echo "2\n" | gmx trjconv -s analysis_tpr_fitted.tpr  -f bigtraj_fit.xtc -drop fma_proteinheavy_all.xvg -dropunder -10.0 -dropover -0.10  -dropmonotonic -o traj_filt_low_proteinheavy.xtc

Last written: frame   1809

echo "2\n" |  gmx trjconv -s analysis_tpr_fitted.tpr  -f bigtraj_fit.xtc -drop fma_proteinheavy_all.xvg -dropunder 1.18453 -dropover 10.0 -dropmonotonic -o traj_filt_high_proteinheavy.xtc

Last written: frame   2022 time 96400.000


vmd proteinheavy.pdb traj_filt_low_proteinheavy.xtc

# For fit of a single frame
set sel0 [atomselect 0 protein]
set sel1 [atomselect 1 protein]
set M [measure fit $sel0 $sel1]
$sel0 move $M

# Set the molecule to their average positions
set sel0 [atomselect 0 protein]
set pos0 [measure avpos $sel0]
set sel1 [atomselect 1 protein]
set pos1 [measure avpos $sel1]
$sel0 set {x y z} $pos0 ; 
$sel1 set {x y z} $pos1 ; 

set sel0 [atomselect 0 protein] 	 
set sel1 [atomselect 1 protein] 	 
set M [measure fit $sel0 $sel1] 	 
$sel0 move $M

$sel0 writepdb cv_protheavy_low_avg.pdb 
$sel1 writepdb cv_protheavy_high_avg.pdb

/opt/UCSF/Chimera64-1.7/bin/chimera cv_protheavy_low_avg.pdb  cv_protheavy_high_avg.pdb


for pt in {0..14}; do; 
    pointnum=$(printf "%04d" pt)
    for i in {0..39}; do;
        shardnum=$(printf "%04d" i)
        
        echo "0\n" | gmx trjconv -f ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/titr_s"$shardnum".xtc -pbc mol -s analysis_tpr_fitted.tpr -o ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/titr_s"$shardnum"_pbc.xtc

        echo "4\n0" | gmx trjconv -f ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/titr_s"$shardnum"_pbc.xtc -fit rot+trans -s analysis_tpr_fitted.tpr -o ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/titr_s"$shardnum"_fit.xtc
        
        echo "4\n2" | gmx anaeig -s analysis_tpr_fitted.tpr -f ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/titr_s"$shardnum"_fit.xtc -v fma_vect_proteinheavy.trr -proj ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/proj_proteinheavy_titr_s"$shardnum".xvg -first 1 -last 1

    done;
done;


For binned titration:

-10.0 to 0.0
0.0 to 0.25
0.25 to 0.50
0.50 to 0.75
0.75 to 10.0


LD_LIBRARY_PATH=/home/ebriand/local/gcc-11/lib64:/home/ebriand/local/gcc-11/lib64/:/home/ebriand/local/lib/::/usr/local/lib/:/usr/local/lib64/:/usr/local/intel/ics/composer_xe_2011_sp1.6.233/mkl/lib/intel64/ ionice -c 3 /home/ebriand/Projects/LambdaAnalysis/cmake-build-relwithdebinfo-gcc-11/LambdaAnalysis titration --runfile /home/ebriand/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2.yaml --output fma_1_asp59_a99_lyzozyme_fmm_2.csv --do_transition_count 1 --filter_timeserie_prefix proj_proteinheavy_titr_s --filter_timeserie_min -10.0 --filter_timeserie_max 0.0

LD_LIBRARY_PATH=/home/ebriand/local/gcc-11/lib64:/home/ebriand/local/gcc-11/lib64/:/home/ebriand/local/lib/::/usr/local/lib/:/usr/local/lib64/:/usr/local/intel/ics/composer_xe_2011_sp1.6.233/mkl/lib/intel64/ ionice -c 3 /home/ebriand/Projects/LambdaAnalysis/cmake-build-relwithdebinfo-gcc-11/LambdaAnalysis titration --runfile /home/ebriand/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2.yaml --output fma_2_asp59_a99_lyzozyme_fmm_2.csv --do_transition_count 1 --filter_timeserie_prefix proj_proteinheavy_titr_s --filter_timeserie_min 0.0 --filter_timeserie_max 0.25

LD_LIBRARY_PATH=/home/ebriand/local/gcc-11/lib64:/home/ebriand/local/gcc-11/lib64/:/home/ebriand/local/lib/::/usr/local/lib/:/usr/local/lib64/:/usr/local/intel/ics/composer_xe_2011_sp1.6.233/mkl/lib/intel64/ ionice -c 3 /home/ebriand/Projects/LambdaAnalysis/cmake-build-relwithdebinfo-gcc-11/LambdaAnalysis titration --runfile /home/ebriand/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2.yaml --output fma_3_asp59_a99_lyzozyme_fmm_2.csv --do_transition_count 1 --filter_timeserie_prefix proj_proteinheavy_titr_s --filter_timeserie_min 0.25 --filter_timeserie_max 0.50

LD_LIBRARY_PATH=/home/ebriand/local/gcc-11/lib64:/home/ebriand/local/gcc-11/lib64/:/home/ebriand/local/lib/::/usr/local/lib/:/usr/local/lib64/:/usr/local/intel/ics/composer_xe_2011_sp1.6.233/mkl/lib/intel64/ ionice -c 3 /home/ebriand/Projects/LambdaAnalysis/cmake-build-relwithdebinfo-gcc-11/LambdaAnalysis titration --runfile /home/ebriand/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2.yaml --output fma_4_asp59_a99_lyzozyme_fmm_2.csv --do_transition_count 1 --filter_timeserie_prefix proj_proteinheavy_titr_s --filter_timeserie_min 0.50 --filter_timeserie_max 0.75

LD_LIBRARY_PATH=/home/ebriand/local/gcc-11/lib64:/home/ebriand/local/gcc-11/lib64/:/home/ebriand/local/lib/::/usr/local/lib/:/usr/local/lib64/:/usr/local/intel/ics/composer_xe_2011_sp1.6.233/mkl/lib/intel64/ ionice -c 3 /home/ebriand/Projects/LambdaAnalysis/cmake-build-relwithdebinfo-gcc-11/LambdaAnalysis titration --runfile /home/ebriand/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2.yaml --output fma_5_asp59_a99_lyzozyme_fmm_2.csv --do_transition_count 1 --filter_timeserie_prefix proj_proteinheavy_titr_s --filter_timeserie_min 0.75 --filter_timeserie_max 10.0







for lower_range in $(seq 0.0 .25 0.74)
do                              
     upper_range=`printf '%f + 0.25\n' "$lower_range" | bc`                                                                                                    
     echo "=======" "$lower_range" to "$upper_range"

     echo LD_LIBRARY_PATH=$LPREFIX/gcc-11/lib64:$LD_LIBRARY_PATH:/usr/local/lib/:/usr/local/lib64/:/usr/local/intel/ics/composer_xe_2011_sp1.6.233/mkl/lib/intel64/ ionice -c 3 \
       /home/ebriand/Projects/LambdaAnalysis/cmake-build-relwithdebinfo-gcc-11/LambdaAnalysis titration \
       --runfile /home/ebriand/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2.yaml \
       --output fma_7_a99_lyzozyme_fmm_2.csv \
       --do_transition_count 1 \
       --filter_timeserie_prefix proj_titr_s \
       --filter_timeserie_min "$lower_range"   \
       --filter_timeserie_max "$upper_range"
done


gmx anaeig -s analysis_tpr.tpr -f ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_0005/titr_s0000_fit.xtc -v fma_vect_proteinheavy.trr -proj ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_"$pointnum"/proj_backbone_titr_s"$shardnum".xvg -first 1 -last 1



gmx anaeig -s analysis_tpr.tpr   -f ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_0005/titr_s0000.xtc -v fma_vect_proteinheavy.trr -filt filt-backbone.xtc -first 1 -last 1

cardiotoxin_pt0006_s0002_fma_proteinheavy_all_singlept

for shard in {1..4}; do; 
     shardnum=$(printf "%04d" shard)
    echo "4\n2" | gmx anaeig -s analysis_tpr_fitted.tpr -f ~/Projects/__CPH_Runs/FinalTitrations/a99_lyzozyme_fmm_2/point_0005/titr_s"$shardnum"_fit.xtc -v fma_vect_proteinheavy.trr -proj cardiotoxin_asp59_pt0006_s"$shardnum"_fma_proteinheavy_all_singlept.xvg -first 1 -last 1 
done;

echo "4\n2" | gmx anaeig -s analysis_tpr_fitted.tpr -f bigtraj_fit.xtc -v fma_vect_proteinheavy.trr -proj cardiotoxin_asp59_allrepl_fma_proteinheavy_all.xvg -first 1 -last 1

```
