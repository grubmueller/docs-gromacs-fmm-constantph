---
title: "Post-processing of Constant pH simulations"
date: 2017-03-02T12:00:00-05:00
---


<div class="maintitle">Post-processing of constant pH simulations</div>


After running a constant pH simulation, you will likely want to analyze it for insight into the protonation dynamics of your protein of interest.

**TODO**: Better intro

- [Analysis tool]({{< relref "/docs/cph-postprocessing/analysis_tool" >}}) to post-process the `.dat` file from a CPH simulation
- [Functional Mode Analysis]({{< relref "/docs/cph-postprocessing/fma" >}}) to analyze the connection between structure change and protonation change

