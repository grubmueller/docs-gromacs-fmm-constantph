---
title: "Implementation details"
date: 2017-03-02T12:00:00-05:00
---

<div class="maintitle">Implementation details</div>

<div class="box_note">
<div class="box_note_heading">Website under construction</div>
<div class="box_note_text">
Please note that this page is currently a work in progress.
</div>
</div>


Relevant implementation details of our FMM and constant pH are documented here.


<figure class="general_figure" style="width: 60%">
{{< baseimg1 "/images/SplineBias.png" "general_figure_image" "Double well potential with spline anchor point visible" "90%" "block" >}}
    <figcaption class="general_figure_caption">
    <span class="general_figure_caption_head" >Hermite Spline-based double-well potential</span>
    </figcaption>
</figure>
