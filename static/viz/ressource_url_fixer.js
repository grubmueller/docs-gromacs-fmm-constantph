function getImgRessourceUrl(name) {
    var prefix = "/docs-gromacs-fmm-constantph/viz-img/";
    return prefix + name;
}

function getDataUrl(name) {
    var prefix = "/docs-gromacs-fmm-constantph/viz/";
    return prefix + name;
}
