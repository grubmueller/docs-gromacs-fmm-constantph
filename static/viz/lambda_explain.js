

function create_lambda_explain(height) {
    
    console.log("Lambda explain");

    const width =  d3.select("body").node().getBoundingClientRect().width;
    const margin_x = 0;
    const margin_y = 0;

    var gaussian_height = 4;
    var gaussian_width = 0.5;

    var show_markings = true;
    var show_lambda_control = true;
    var show_dvdl = true;
    var show_bias = true;
    var doThermalNoise = true;
    
    var lambda_prot = 0.0;
    var lambda_taut = 1.0;
    var e_clash = 0.0;
    var isNonCPH = false;
    
    var thermalVar = 0.05;
    var variance = thermalVar;
    
    // Global hoisting
    var scaleXBias;
    var scaleYBias;
    var biasInterpolator;
    var unitToLambda;

    var	svg = d3.select("body")
        .append("svg")
        .attr("width",width)
        .attr("height", height - margin_y)
        .attr("viewBox", [0, 200, 1300,300 ])
//         .attr("width", width + margin_x)
//         .attr("height", height + margin_y)
        .attr("fill", "none")
        .append("g");

        
    var text_g = svg.append("g");
    var legend_text = text_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("legend_text_1.svg"))
        .attr("width", 400)
        .attr("x", 160)
        .attr("y", 400)
        .style("opacity", _ => (isNonCPH ? 1.0 : 0.0));

    var background_prot = svg.append("image")
        .attr("xlink:href", getImgRessourceUrl("model_protein_with_cv_nomarkings.png"))
        .attr("width", 400)
        .attr("x", 100)
        .attr("y", 0);
        
    var carboxylate_g = svg.append("g");
    var carboxylate_current_angle = 240;
    var carboxylate_angle_offset = 0;
    var carboxylate_translate_x = 300;
    var carboxylate_translate_y = 250;
    var carboxylate_rotate_offset_x = 20;
    var carboxylate_rotate_offset_y = 10;
    
    var glu_deprot_img = carboxylate_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("glutamicacid_deprotonated.svg"))
        .attr("width", 150)
        .attr("x", 0)
        .attr("y", 0)
        .style("opacity", 1.0 - lambda_prot);
        
    var glu_prot1_img = carboxylate_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("glutamicacid_protonated.svg"))
        .attr("width", 150)
        .attr("x", 0)
        .attr("y", 0)
        .style("opacity", lambda_prot * (lambda_taut));
    
    var glu_prot2_img = carboxylate_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("glutamicacid_protonated_2.svg"))
        .attr("width", 150)
        .attr("x", 0)
        .attr("y", 0)
        .style("opacity", lambda_prot * (1.0 - lambda_taut));
        
    var e_clash_img = carboxylate_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("e_clash.svg"))
        .attr("width", 150)
        .attr("x", 0)
        .attr("y", 0)
        .style("opacity", e_clash);
    

        
    function update_lambda() {
        if(!isNonCPH) {
                glu_deprot_img.transition()
            .duration(50)
            .style("opacity", 1.0 - lambda_prot);
            
        glu_prot1_img.transition()
        .duration(50)
        .style("opacity", lambda_prot * (lambda_taut));
        
        glu_prot2_img.transition()
        .duration(50)
        .style("opacity", lambda_prot * (1.0 - lambda_taut));
            
        } else {
            
            lambda_prot = 0.0;
            lambda_taut = 0.0;
            
        glu_deprot_img.transition()
            .duration(50)
            .style("opacity", 1.0 - lambda_prot);
            
        glu_prot1_img.transition()
        .duration(50)
        .style("opacity", lambda_prot * (lambda_taut));
        
        glu_prot2_img.transition()
        .duration(50)
        .style("opacity", lambda_prot * (1.0 - lambda_taut));
            
            
        }
        

    }
        
        
    carboxylate_g.attr("transform", `translate(${carboxylate_translate_x},${carboxylate_translate_y}) rotate(${180 - carboxylate_current_angle + carboxylate_angle_offset} ${carboxylate_rotate_offset_x} ${carboxylate_rotate_offset_y})`);

    var marking_bitmap = svg.append("image")
        .attr("xlink:href", getImgRessourceUrl("axis_no_markings.png"))
        .attr("width", 400)
        .attr("x", 100)
        .attr("y", 50)
        //.attr('visibility', true);
        .attr('visibility', _ => (show_markings ? "visible" : "hidden"));
        
    
    var lambda_p_text = svg.append("image")
        .attr("xlink:href", getImgRessourceUrl("text_lambda_p.svg"))
        .attr("width", 400)
        .attr("x", 600)
        .attr("y", 00)
        //.attr('visibility', true);
        .attr('visibility', _ => (show_lambda_control ? "visible" : "hidden"));
        
    
    
    var n = 100;
    var lambdaGraphWidth = 300;
    var lambdaGraphHeight = 200;
    var random = d3.randomNormal(lambda_prot, variance);
    
    lambdaProtData = d3.range(n).map(random);
    
    var lambdaProtXScale = d3.scaleLinear()
    .domain([0, n - 1])
    .range([0, lambdaGraphWidth]);
    
    var lambdaProtYScale = d3.scaleLinear()
    .domain([-0.2, 1.2])
    .range([lambdaGraphHeight, 0]);
    
    
    lambdaProtYScale.ticks(13);
    
    var lambdaProtLine = d3.line()
        .x(function(d, i) { return lambdaProtXScale(i); })
        .y(function(d, i) { return lambdaProtYScale(d); });
    
    var lambdaProtG = svg.append("g");
    lambdaProtG.attr("transform", `translate(${610},${30})`);
    lambdaProtG.attr('visibility', _ => (show_lambda_control ? "visible" : "hidden"));

    
    
    var genAxisProt = lambdaProtG.append("image")
        .attr("xlink:href", getImgRessourceUrl("generic_axis_v.png"))
        .attr("width", 23)
        .attr("x", lambdaGraphWidth+20)
        .attr("y", 0);
    
    var lambdaProtG_dot_g = lambdaProtG.append("g");
    
    var lambdaProtG_dot_y_translate_offset = 30;
    var lambdaProtG_dot_y_translate_max = 140;
    var lambdaProtG_dot_y_translate = lambdaProtG_dot_y_translate_max + lambdaProtG_dot_y_translate_offset;
    
    lambdaProtG_dot_g.attr("transform", `translate(${lambdaGraphWidth+30},${lambdaProtG_dot_y_translate})`);
    var lambdaProt_handle = lambdaProtG_dot_g.append('circle')
        .classed('dotmoverLambdaProt', true)
        .attr('r', 10)
        .attr('stroke', 'black')
        .attr('fill', 'green')
        .attr('visibility', _ => (show_lambda_control ? "visible" : "hidden"));
    
    var	lambdaProtScaleYPosToLambda = d3.scaleLinear()
        .domain([lambdaProtG_dot_y_translate_max + lambdaProtG_dot_y_translate_offset, lambdaProtG_dot_y_translate_offset])
        .range([0.0, 1.0]);
        
    var	lambdaProtScaleYPosToLambda_reverse = d3.scaleLinear()
        .range([lambdaProtG_dot_y_translate_max + lambdaProtG_dot_y_translate_offset, lambdaProtG_dot_y_translate_offset])
        .domain([0.0, 1.0]);
    
    function updateLambdaForced() {
        lambdaProtG_dot_g.attr("transform", `translate(${lambdaGraphWidth+30},${lambdaProtG_dot_y_translate})`);
        lambda_prot = Math.max(0.0, Math.min(1.0, lambdaProtScaleYPosToLambda(lambdaProtG_dot_y_translate)));
         svg.select(".biasParticle").attr("cx", scaleXBias(biasInterpolator(unitToLambda( lambdaProtScaleYPosToLambda(lambdaProtG_dot_y_translate)))))
        .attr("cy", scaleYBias( lambdaProtScaleYPosToLambda(lambdaProtG_dot_y_translate)))
        update_lambda();
    }
        
    function externalUpDateLambdaForced() {
        lambdaProtG_dot_y_translate = lambdaProtScaleYPosToLambda_reverse(lambda_prot);
        lambdaProtG_dot_g.attr("transform", `translate(${lambdaGraphWidth+30},${lambdaProtG_dot_y_translate})`);
        svg.select(".biasParticle").attr("cx", scaleXBias(biasInterpolator(unitToLambda( lambdaProtScaleYPosToLambda(lambdaProtG_dot_y_translate)))))
        .attr("cy", scaleYBias( lambdaProtScaleYPosToLambda(lambdaProtG_dot_y_translate)))
    }
    

    
    
    var drag_behav_lp_handle = d3.drag()
        .on("start", _ => { })
        .on("drag", event => {
            lambdaProtG_dot_y_translate =  Math.max(lambdaProtG_dot_y_translate_offset, Math.min(lambdaProtG_dot_y_translate_max + lambdaProtG_dot_y_translate_offset, event.y )) ;
            updateLambdaForced();
        })
        .on("end", _ => { });

    drag_behav_lp_handle(lambdaProtG_dot_g);
    
    
    
    
    lambdaProtG.append("defs").append("clipPath")
        .attr("id", "clippathlambdaprot")
        .append("rect")
        .attr("width", lambdaGraphWidth)
        .attr("height", lambdaGraphHeight);
    
   
    
    var lambdaProtYAxis = lambdaProtG.append("g")
    .attr("class", "axis axis--y")
    .call(d3.axisLeft(lambdaProtYScale).tickValues([0.0, 0.5, 1.0]));
    
    
    var lambdaProtGraph = lambdaProtG.append("g")
        .attr("clip-path", "url(#clippathlambdaprot)")
        .append("path")
            .datum(lambdaProtData)
            .attr("class", "lambdaline")
        .transition()
            .duration(60)
            .ease(d3.easeLinear)
            .on("start", lambdaProtTick);
            
        
    function lambdaProtTick() {
        var random = d3.randomNormal(lambda_prot, variance);
        
        // Push a new data point onto the back.
        lambdaProtData.push(random());

          d3.select(this)
            .attr("d", lambdaProtLine)
            .attr("transform", null);
        
          d3.active(this)
            .attr("transform", "translate(" + lambdaProtXScale(-1) + ",0)")
            .transition()
            .on("start", lambdaProtTick);
            
            
        // Pop the old data point off the front.
        lambdaProtData.shift();


    }
    
    
    var biasPlotWidth = 200;
    var biasPlotHeight = 200
    var bias_g = lambdaProtG.append("g");
    bias_g.attr('visibility', _ => (show_bias ? "visible" : "hidden"));
    bias_g.attr("transform", `translate(${400},${-10})`);
    
    var particle_g = bias_g.append("g");
    
    d3.csv(getDataUrl("ld_bias.csv")).then( function(data) {
        
        for (let i = 0; i < data.length; i++) {
            data[i].lambda = parseFloat(data[i].lambda);
            data[i].bias = parseFloat(data[i].bias);
            data[i].force = parseFloat(data[i].force);
        }
        
        scaleXBias = d3.scaleLinear()
            .domain([d3.min(data.map(d => d.bias)),
                d3.max(data.map(d => d.bias))])
            .range([0.0, biasPlotWidth]);
        
        var scaleX_rev_unit = d3.scaleLinear()
            .range([d3.min(data.map(d => d.bias)),
                d3.max(data.map(d => d.bias))])
            .domain([0.0, 1.0]);
            
        var scaleX_reverse = d3.scaleLinear()
            .range([d3.min(data.map(d => d.bias)),
                d3.max(data.map(d => d.bias))])
            .domain([0.0, biasPlotWidth]);
        
        scaleYBias = d3.scaleLinear()
            .domain([d3.min(data.map(d => d.lambda)),
                d3.max(data.map(d => d.lambda))])
             .range([biasPlotHeight, 0.0]);
             
        var scaleY_reverse = d3.scaleLinear()
            .domain([d3.min(data.map(d => d.lambda)),
                d3.max(data.map(d => d.lambda))])
             .range([biasPlotHeight, 0.0]);
        
        var interpolatedLine = d3.line()
            .curve(d3.curveBasis)
            .x(d => scaleXBias(d.bias))
            .y(d => scaleYBias(d.lambda))
        
        biasInterpolator = d3.interpolateBasis(data.map(d => d.bias));
        unitToLambda = d3.scaleLinear()
            .domain([d3.min(data.map(d => d.lambda)),
                d3.max(data.map(d => d.lambda))])
            .range([0.0, 1.0]);
            
        var forceInterp = d3.line()
            .curve(d3.curveBasis)
            .x(d => scaleYBias(d.lambda))
            .y(d => d.force)
             
        var bias_curve = bias_g.append("path")
            .attr("class", "line")
            .attr("stroke", "black")
            .attr("stroke-width", "5")
            .attr("d", interpolatedLine(data));
            
        console.log(biasInterpolator(0.0))
        console.log(biasInterpolator(1.0))
             
        var biasParticle = bias_g.append('circle')
        .classed('biasParticle', true)
        .attr('r', 10)
        .attr("cx", scaleXBias(biasInterpolator(unitToLambda(0.0))))
        .attr("cy", scaleYBias(0.0))
        .attr('stroke', 'black')
        .attr('fill', 'green')
        .attr('visibility', true);
            
    });
    
    
    
    var dvdl_g = svg.append("g");
    dvdl_g.attr('visibility', _ => (show_dvdl ? "visible" : "hidden"));

    dvdl_g.attr("transform", `translate(${0},${460})`);
    dvdl_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("FHinterp_is_potsubstration.png"))
        .attr("height", 150)
        .attr("x", 0)
        .attr("y", -20)
        .attr('visibility', true);
        
    
        

        
    
        
    var helix_g = svg.append("g");
    var helix_angle_peak2 = 50;
    var helix_current_angle = helix_angle_peak2;
    var angle_offset = -115;
    var helix_translate_x = 195;
    var helix_translate_y = 145;
    var helix_rotate_offset_x = 20;
    var helix_rotate_offset_y = 10;

    var alpha_helix = helix_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("A-Helix.png"))
        .attr("height", 200)
        .attr("x", 0)
        .attr("y", 0);
        
    var minus_1 = helix_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("minus.svg"))
        .attr("height", 15)
        .attr("x", 52)
        .attr("y", 130);
        
    var minus_2 = helix_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("minus.svg"))
        .attr("height", 15)
        .attr("x", 35)
        .attr("y", 110);
        
    var minus_3 = helix_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("minus.svg"))
        .attr("height", 15)
        .attr("x", 55)
        .attr("y", 150);
        
        
    var e_clash_img_2 = helix_g.append("image")
        .attr("xlink:href", getImgRessourceUrl("e_clash_2.svg"))
        .attr("width", 150)
        .attr("x", 30)
        .attr("y", 100)
        .style("opacity", e_clash);

    helix_g.attr("transform", `translate(${helix_translate_x},${helix_translate_y}) rotate(${180 - helix_current_angle + angle_offset} ${helix_rotate_offset_x} ${helix_rotate_offset_y})`);

    


    var dot_g = svg.append("g");
    var dot_translate_x_min = 145;
    var dot_translate_x_max = 412;
    var dot_translate_x_peak1 = 180;
    var dot_translate_x_peak2 = 357;
    var dot_translate_x_orig = 145;
    var dot_translate_x = dot_translate_x_orig;
    var dot_translate_y_orig = 385;
    var dot_translate_y = dot_translate_y_orig;
    dot_g.attr("transform", `translate(${dot_translate_x},${dot_translate_y}) `);

    var dot_cicle = dot_g.append('circle')
        .classed('dotmover', true)
        .attr('r', 15)
        .attr('stroke', 'black')
        .attr('fill', 'yellow')
        //.attr('visibility', true);
        .attr('visibility', _ => (show_markings ? "visible" : "hidden"));

    var	scaleXPosToAngle = d3.scaleLinear()
        .domain([dot_translate_x_min, dot_translate_x_peak1, dot_translate_x_peak2, dot_translate_x_max])
        .range([ -70, -20, 90, 100]);
        
    var startToFeelChargeAngle = 60;
    var	scaleAngleToLambda = d3.scaleLinear()
        .domain([-70, startToFeelChargeAngle, 60, 80])
        .range([ 0.0, 0.0, 0.2, 1.0]);  

    var	scaleAngleToEclash = d3.scaleLinear()
        .domain([-70, startToFeelChargeAngle - 20, 60, 80])
        .range([ 0.0, 0.001, 1.0, 0.0]);  
        
    var	scaleAngleToEclashNonCPH = d3.scaleLinear()
        .domain([-70, startToFeelChargeAngle - 20, 60, 80])
        .range([ 0.0, 0.001, 1.0, 1.0]);  

    function update_helix_dot_animated() {
        var old_cx = dot_cicle.attr("cx");
        dot_g.transition()
            .duration(500)
            .attr("transform", `translate(${dot_translate_x},${dot_translate_y}) `);
        helix_current_angle = scaleXPosToAngle(dot_translate_x);
        helix_g.transition()
            .duration(500)
            .attr("transform", `translate(${helix_translate_x},${helix_translate_y}) rotate(${180 - helix_current_angle + angle_offset} ${helix_rotate_offset_x} ${helix_rotate_offset_y})`);
        

            
    }



    var drag_behav = d3.drag()
        .on("start", _ => { })
        .on("drag", event => {
            //console.log( event.x)
            if(event.x < dot_translate_x_min) {
                dot_translate_x = dot_translate_x_min;
            } else if(event.x > dot_translate_x_max) {
                dot_translate_x = dot_translate_x_max;
            } else {
                dot_translate_x = event.x;
            }
            update_helix_dot();
        })
        .on("end", _ => { });

    drag_behav(dot_g);


        

    //var ui_g = svg.append("g");
    var ui_translate_x = 400;
    var ui_translate_y = 0;
    //ui_g.attr("transform", `translate(${ui_translate_x},${ui_translate_y})`);


//     var show_marking_rect = ui_g.append("rect")
//         .attr("x", 0)
//         .attr("y", 0)
//         .attr("width", 10)
//         .attr("height", 10)
//         .attr("stroke", "black")
//         .attr("fill", "blue")
//         .on("click", function(){
//             if(show_lambda_control) {
//                 show_lambda_control = false;
//                 show_marking_rect.attr("stroke", "black");
//             } else {
//                 show_lambda_control = true;
//                 show_marking_rect.attr("stroke", "none");
//             }
//             
//         lambdaProtG.attr('visibility', _ => (show_lambda_control ? "visible" : "hidden"));
//         lambdaProt_handle.attr('visibility', _ => (show_lambda_control ? "visible" : "hidden"));
//         lambda_p_text.attr('visibility', _ => (show_lambda_control ? "visible" : "hidden"));
// 
//         })
//     ;

//     var show_dvdl_rect = ui_g.append("rect")
//         .attr("x", 15)
//         .attr("y", 0)
//         .attr("width", 10)
//         .attr("height", 10)
//         .attr("stroke", "black")
//         .attr("fill", "grey")
//         .on("click", function(){
//             if(show_dvdl) {
//                 show_dvdl = false;
//                 show_dvdl_rect.attr("stroke", "black");
//             } else {
//                 show_dvdl = true;
//                 show_dvdl_rect.attr("stroke", "none");
//             }
//             
//             dvdl_g.attr('visibility', _ => (show_dvdl ? "visible" : "hidden"));
// 
//             
//         })
//     ;

    // Definition hoisting
    var scaleX_reverse = {};
    var sim_config = {};
    var interpolated_line = {};
    var interpolated_line_bias = {};

//     var show_bias_rect = ui_g.append("rect")
//     .attr("x", 30)
//     .attr("y", 0)
//     .attr("width", 10)
//     .attr("height", 10)
//     .attr("stroke", "black")
//     .attr("fill", "red")
//     .on("click", function(){
//             if(show_bias) {
//                 show_bias = false;
//                 show_bias_rect.attr("stroke", "black");
//             } else {
//                 show_bias = true;
//                 show_bias_rect.attr("stroke", "none");
//             }
//             
//         bias_g.attr('visibility', _ => (show_bias ? "visible" : "hidden"));
//         
//     });

    
//     var show_thermal_rect = ui_g.append("rect")
//         .attr("x", 45)
//         .attr("y", 0)
//         .attr("width", 10)
//         .attr("height", 10)
//         .attr("stroke", "black")
//         .attr("fill", "yellow")
//         .on("click", function(){
//             if(doThermalNoise) {
//                 doThermalNoise = false;
//                 show_thermal_rect.attr("stroke", "black");
//             } else {
//                 doThermalNoise = true;
//                 show_thermal_rect.attr("stroke", "none");
//             }
//             if(doThermalNoise) {
//                 variance = thermalVar;
//              } else {
//                 variance = 0.0;
//             }
//         });

    
    var potlandscape_translate_y = dot_translate_y + 50;
    var potlandscape_height = 80;

    
    var y_range = 20;
    var y_var  = 2;
    var y_mean = -5;
    var x_num_value = 50;
    var current_i = 1;

    var	scaleYMean = d3.scalePow()
        .exponent(5)
        .domain([dot_translate_x_min, 335])
        .range([-y_range, y_range]).clamp(false);
    
    var lineData = [{"x": 0, "y":y_mean}];
    y_mean = scaleYMean(dot_translate_x)

    

    function update_helix_dot() {
        dot_g.attr("transform", `translate(${dot_translate_x},${dot_translate_y}) `);
        helix_current_angle = scaleXPosToAngle(dot_translate_x);
        helix_g.attr("transform", `translate(${helix_translate_x},${helix_translate_y}) rotate(${180 - helix_current_angle + angle_offset} ${helix_rotate_offset_x} ${helix_rotate_offset_y})`);
        y_mean = 2*scaleYMean(dot_translate_x);
        //console.log("tr_x", dot_translate_x)
        //console.log("y_mean", y_mean)
        
        lambda_prot =  Math.min(1.0,scaleAngleToLambda(helix_current_angle));
        if(isNonCPH) {
            e_clash =  scaleAngleToEclashNonCPH(helix_current_angle);
        }else {
            e_clash =  scaleAngleToEclash(helix_current_angle);
        }
        
        e_clash_img.transition()
            .duration(50)
            .style("opacity", e_clash);
        e_clash_img_2.transition()
            .duration(50)
            .style("opacity", e_clash);
        update_lambda();
        externalUpDateLambdaForced();
    }
    
    update_helix_dot();
    




}
